import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper'

import { AppLoader } from 'components/Loader'

const locationHelper = locationHelperBuilder({})

const authOnlyDefaults = {
  authenticatedSelector: state =>
    state.auth.get('isAuthenticated') && !state.auth.get('isAuthenticating'),
  authenticatingSelector: state => state.auth.get('isAuthenticating'),
  wrapperDisplayName: 'UserIsAuthenticated'
}

export const AuthOnly = connectedAuthWrapper(authOnlyDefaults)

export const AuthOnlyRedir = connectedRouterRedirect({
  ...authOnlyDefaults,
  AuthenticatingComponent: AppLoader,
  redirectPath: '/login'
})

const publicOnlyDefaults = {
  // Want to redirect the user when they are done loading and authenticated
  authenticatedSelector: state =>
    !state.auth.get('isAuthenticated') && !state.auth.get('isAuthenticating'),
  wrapperDisplayName: 'UserIsNotAuthenticated'
}

export const PublicOnly = connectedAuthWrapper(publicOnlyDefaults)

export const PublicOnlyRedir = connectedRouterRedirect({
  ...publicOnlyDefaults,
  redirectPath: (state, ownProps) =>
    locationHelper.getRedirectQueryParam(ownProps) || '/',
  allowRedirectBack: false
})
