import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const LoginWrapper = styled.div`
  padding-top: 10rem;
  height: 100%;
  width: 100%;
  background-color: #333758;
`

export const LoginLayout = ({ children }) => (
  <LoginWrapper>
    <div className='ui middle aligned center aligned grid'>{children}</div>
  </LoginWrapper>
)

LoginLayout.propTypes = {
  children: PropTypes.node
}

export default Component => props => (
  <LoginLayout {...props}>
    <Component {...props} />
  </LoginLayout>
)
