import { connect } from 'react-redux'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/roles'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.roles.getOne(state, { id: match.params.id }),
    dependencies: {
      permissions: state.permissions.get('list').toList()
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    loading:
      state.loading.effects.permissions.fetchAll ||
      state.loading.effects.roles.fetch,
    saving: state.loading.effects.roles.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = { with: 'permissions' }
  return {
    saveAction: role => dispatch.roles.update(role),
    prefetch: [
      () => dispatch.roles.fetch({ id, params }),
      () => dispatch.permissions.fetchAll()
    ]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceEditView)
