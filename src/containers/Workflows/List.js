import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/workflows'
import EditContainer from './Edit'
import CreateContainer from './New'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.workflows.getItems(state),
    params: store.select.workflows.getParams(state),
    resourceName,
    resourceList,
    slideoutWidth: 'calc(100vw - 76px)',
    detailView: EditContainer,
    createView: CreateContainer,
    createOptions: [
      { label: 'Simple Mode', key: 'simple' },
      { label: 'Advanced Mode', key: 'advanced' }
    ],
    loading: state.loading.effects.workflows.fetchAll
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.workflows.fetchAll()],
    refetch: () => dispatch.workflows.fetchAll(),
    removeAction: id => dispatch.workflows.remove(id),
    changeParams: filters => dispatch.workflows.changeParams(filters),
    resetParams: () => dispatch.workflows.resetParams(),
    shortcutActions: item => [
      {
        label: 'Clone',
        onClick: () => dispatch.workflows.duplicate(item.id),
        icon: 'clone outline'
      }
    ]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
