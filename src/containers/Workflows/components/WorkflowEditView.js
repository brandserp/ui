import React from 'react'
import PropTypes from 'prop-types'
import { Form } from 'semantic-ui-react'
import { Drawer, Select, Switch } from 'antd'

import { ResourceEditView } from 'components/Views'
import WorkflowEditor from 'components/WorkflowEditor'

const Option = Select.Option

class WorkflowEditView extends ResourceEditView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      isDrawerOpen: false,
      form: {
        id: props.item ? props.item.id : '',
        name: props.item ? props.item.name : '',
        documentTypeId: props.item ? props.item.documentTypeId : '',
        published: props.item ? props.item.published : false,
        deploymentId: props.item ? props.item.deploymentId : null,
        bpmnXml: props.item ? props.item.bpmnXml : null,
        bpmnId: props.item ? props.item.bpmnId : null,
        options: props.item ? props.item.options : null
      }
    }
  }

  updateForm = e => {
    const data = {}
    data['documentTypeId'] = e
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateOptions = e => {
    const { options } = this.state.form
    options['startInstanceOnUpload'] = !options['startInstanceOnUpload']
    this.setState({
      form: {
        ...this.state.form,
        ...{ options }
      }
    })
  }

  onSave = ({ xml, defs }) => {
    const { name, id } = defs.rootElements[0]
    const { saveAction, item } = this.props
    const newWorkflow = {
      id: item.id,
      bpmnXml: xml,
      bpmnId: id,
      name: name || id
    }
    saveAction(newWorkflow).then(() => {
      this.context.router.history.push(`/workflows/${item.id}`)
    })
  }

  onPublish = () => {
    const { item, publish } = this.props
    publish(item.id)
  }

  onUnpublish = () => {
    const { item, unpublish } = this.props
    unpublish(item.id)
  }

  cloneWorkflow = () => {
    const { duplicate, item } = this.props
    duplicate(item.id).then(data => {
      this.context.router.history.push(`/workflows/edit/${data.id}`)
    })
  }

  onSave = ({ xml, defs }) => {
    const { name, id } = defs.rootElements[0]
    const { form } = this.state
    const { saveAction, resourceName } = this.props
    const newWorkflow = {
      ...form,
      bpmnXml: xml,
      bpmnId: id,
      name: name || id
    }
    saveAction(newWorkflow)
  }

  openDrawer = () =>
    this.setState({
      isDrawerOpen: true
    })

  closeDrawer = () =>
    this.setState({
      isDrawerOpen: false
    })

  renderContent = () => {
    const { isDrawerOpen, form } = this.state
    const { item, dependencies } = this.props
    const documentTypeOps = dependencies.documentTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    const isValid = form.documentTypeId
    return (
      <div className='workflow-container'>
        <Drawer
          onClose={this.closeDrawer}
          visible={isDrawerOpen}
          title='Workflow Settings'
          closable
          width={320}
        >
          <div style={{ width: '100%' }}>
            <label style={{ fontWeight: 'bold', color: 'primary' }}>
              Start a document type
            </label>
            <Select
              showSearch
              style={{ width: '100%', marginBottom: 30 }}
              value={form.documentTypeId}
              placeholder='Choose a document type'
              optionFilterProp='children'
              onChange={this.updateForm}
              filterOption={(input, option) =>
                option.props.children
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
            >
              {documentTypeOps.map((v, k) => (
                <Option value={v.value} key={v.id}>
                  {v.text}
                </Option>
              ))}
            </Select>
          </div>
          <Switch
            style={{ width: '100%' }}
            unCheckedChildren='Start new instance on creation'
            checked={form.options.startInstanceOnUpload}
            checkedChildren='Start new instance on creation'
            onChange={this.updateOptions}
          />
        </Drawer>
        <WorkflowEditor
          onSave={isValid ? this.onSave : null}
          published={item.published}
          canPublish={!item.published}
          canDuplicate
          onUnpublish={item.published ? this.onUnpublish : null}
          onPublish={!item.published ? this.onPublish : null}
          onDuplicate={this.cloneWorkflow}
          openDrawer={this.openDrawer}
          workflow={item}
        />
      </div>
    )
  }
}

export default WorkflowEditView
