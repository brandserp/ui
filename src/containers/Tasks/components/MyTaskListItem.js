import React from 'react'
import { Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const MyTaskListItem = ({ item, index, dependencies }) => {
  const { authUser } = dependencies
  return (
    <>
      <Header as='h5'>
        <Ellipsis text={item.name} basedOn='letters' trimRight />
      </Header>
      {`${authUser.firstName} ${authUser.lastName}`}
    </>
  )
}

export default MyTaskListItem
