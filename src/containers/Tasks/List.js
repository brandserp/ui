import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/tasks'
import EditContainer from './Edit'
import CompleteContainer from './Complete'
import itemRenderer from './components/TasksListItem'
import store from 'store'

const mapStateToProps = (state, { location, match }) => {
  const { resourceName, resourceList } = modelConfig
  const authUser = store.select.auth.getAuthUser(state)
  const viewId = new URLSearchParams(location.search).get('view')
  const defaultView = new URLSearchParams(location.search).get('default')
  const view = viewId ? store.select.views.getOne(state, { id: viewId }) : null

  return {
    items: store.select.tasks.getItems(state),
    params: store.select.tasks.getParams(state),
    dependencies: {
      users: state.users.get('list')
    },
    loading:
      state.loading.effects.tasks.fetchAll ||
      state.loading.effects.users.fetchAll ||
      state.loading.effects.views.fetchByType,
    authUser,
    resourceName,
    resourceList,
    detailView: EditContainer,
    completeView: CompleteContainer,
    itemRenderer,
    viewType: 'tasks',
    hideIndex: true,
    customViews: true,
    defaultView,
    view,
    viewId
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
      () => dispatch.users.fetchAll(),
      () => dispatch.views.fetchByType('tasks')
    ],
    changeParams: filters => dispatch.tasks.changeParams(filters),
    shortcutActions: item => [
      {
        label: 'Claim',
        icon: 'hand paper outline',
        onClick: () => {
          dispatch.tasks.claimTask(item.id)
        }
      }
    ],
    refetch: () => dispatch.tasks.fetchAll(),
    resetParams: () => dispatch.tasks.resetParams(),
    saveView: view => dispatch.views.create(view),
    renameView: view => dispatch.views.rename(view),
    deleteView: id => dispatch.views.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
