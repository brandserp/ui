import { connect } from 'react-redux'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/tasks'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.tasks.getOne(state, { id: match.params.id }),
    dependencies: {
      users: state.users
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    loading: state.loading.effects.tasks.fetch,
    saving: state.loading.effects.tasks.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: task => dispatch.tasks.update(task),
    prefetch: [() => dispatch.tasks.fetch({ id })]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceEditView)
