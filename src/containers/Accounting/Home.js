import { connect } from 'react-redux'
import AccountingHomeView from './components/AccountingHomeView'
import store from 'store'

const mapStateToProps = (state, { location, match }) => {
  const authUser = store.select.auth.getAuthUser(state)

  return {
    dependencies: {},
    authUser
  }
}

export default connect(
  mapStateToProps,
  null
)(AccountingHomeView)
