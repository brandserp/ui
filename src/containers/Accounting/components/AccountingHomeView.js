import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PageHeader from 'ant-design-pro/lib/PageHeader'
import Iframe from 'react-iframe'

import styles from './AccountingHomeView.module.scss'

class AccountingHomeView extends Component {
  constructor (props) {
    super(props)
    this.baseRoute = '/accounting'
    this.frames = {
      [`${this.baseRoute}`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_LEDGER',
      [`${this.baseRoute}/transaction/new`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_GENERAL_ACCOUNT_ENTRY',
      [`${this.baseRoute}/receipt/new`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_SIMPLE_RECEIPT_ENTRY',
      [`${this.baseRoute}/statements`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_GENERAL_ACCOUNT_ENTRY',
      [`${this.baseRoute}/rent-charges`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_ENTRY',
      [`${this.baseRoute}/reconcile/entries`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_ACCOUNT_ENTRIES',
      [`${this.baseRoute}/reconcile/statements`]: 'https://erpdev.brandscapital.com/index.php?op=DO_DRAW_STATEMENT_ACCOUNT_ENTRIES'
    }
    this.state = {
      content: null
    }
  }

  static contextTypes = {
    router: PropTypes.object
  }

  render = () => {
    const { location } = this.props
    return (
      <div className='master-list'>
        <div className='list-wrapper'>
          <PageHeader
            title='Accounting'
            hiddenBreadcrumb
          />
          <div className={styles.ledger}>
            <Iframe
              src={`${this.frames[location.pathname]}`}
              id="ledger"
              display="initial"
              position="relative"
            />
          </div>
        </div>
      </div>
    )
  }
}

export default AccountingHomeView
