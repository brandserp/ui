import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documents'
import DocumentListItem from './components/DocumentListItem'
import EditContainer from './Edit'
import CreateContainer from './New'
import store from 'store'

const mapStateToProps = (state, { location, match }) => {
  const { resourceName, resourceList } = modelConfig
  const authUser = store.select.auth.getAuthUser(state)
  const viewId = new URLSearchParams(location.search).get('view')
  const defaultView = new URLSearchParams(location.search).get('default')
  const view = viewId ? store.select.views.getOne(state, { id: viewId }) : null

  return {
    items: store.select.documents.getFilteredItems(state),
    params: store.select.documents.getParams(state),
    dependencies: {
      documentTypes: store.select.documentTypes.getList(state)
    },
    authUser,
    resourceName,
    resourceList,
    customViews: true,
    defaultView,
    view,
    viewId,
    itemRenderer: DocumentListItem,
    slideoutWidth: 768,
    detailView: EditContainer,
    createView: CreateContainer,
    viewType: 'documents',
    loading:
      state.loading.effects.documents.fetchAll ||
      state.loading.effects.views.fetchByType
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    prefetch: [() => dispatch.views.fetchByType('documents')],
    refetch: () => dispatch.documents.fetchAll(),
    changeParams: filters => dispatch.documents.changeParams(filters),
    resetParams: () => dispatch.documents.resetParams(),
    removeAction: id => dispatch.documents.remove(id),
    saveView: view => dispatch.views.create(view),
    updateView: id => dispatch.views.update(id),
    renameView: view => dispatch.views.rename(view),
    deleteView: id => dispatch.views.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
