import { connect } from 'react-redux'

import DocumentEditView from './components/DocumentEditView'
import { modelConfig } from 'models/documents'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.documents.getIn(['list', `${match.params.id}`]),
    dependencies: {
      documentTypes: state.documentTypes.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'container/documents',
    saving: state.loading.effects.documents.update || false,
    loading:
      state.loading.effects.documentTypes.fetchAll ||
      state.loading.effects.documents.fetchPending
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: type => dispatch.documents.update(type),
    prefetch: [
      () => dispatch.documents.fetchPending(id),
      () => dispatch.documentTypes.fetchAll()
    ]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentEditView)
