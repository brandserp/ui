import { connect } from 'react-redux'
import { all as fileTypes } from 'utils/fileTypes'

import DocumentCreateView from './components/DocumentCreateView'
import { modelConfig } from 'models/documents'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      fileTypes,
      documentTypes: state.documentTypes.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'container/documents',
    saving: state.loading.effects.documents.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.documentTypes.fetchAll()],
    saveAction: type => dispatch.documents.create(type)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentCreateView)
