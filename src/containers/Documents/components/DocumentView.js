import React from 'react'
import PropTypes from 'prop-types'
import { Tabs, Radio, Row, Col, Card, Icon, Spin } from 'antd'

import { BaseView } from 'components/Views'
import ProcessTab from './ProcessTab'

const TabPane = Tabs.TabPane

class DocumentView extends BaseView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  state = { mode: 'top' }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      this.setState({
        form: {
          id: nextProps.item.id,
          documentTypeId: nextProps.item.documentTypeId,
          filename: nextProps.item.filename
        }
      })
    }
  }

  handleModeChange = e => {
    const mode = e.target.value
    this.setState({ mode })
  }

  renderContent = () => {
    const {
      resourceName,
      procLoading,
      dependencies,
      item,
      processServices
    } = this.props
    const { mode } = this.state

    const secAction = {
      content: 'View File',
      icon: 'external',
      onClick: () => window.open(item.url)
    }
    const docTypeList = dependencies.documentTypes.toJS()
    const docType = docTypeList[`${item.documentTypeId}`].name
    const procs = dependencies.processes
      .filter(p => item.processes.includes(p.processInstanceId))
      .toList()
    const workflows = dependencies.workflows

    return (
      <div>
        <h1>Document - {`${item.id}`}</h1>
        <Card
          title={
            <h4>
              <Icon type='ordered-list' /> Details
            </h4>
          }
          extra={
            <a onClick={() => window.open(item.url)}>
              View File <Icon type='arrow-right' />
            </a>
          }
        >
          <Row>
            <Col span={8}>
              <h5>FILE NAME</h5>
              <span>{`${item.filename}.${item.extension}`}</span>
            </Col>
            <Col span={8}>
              <h5>MIME TYPE</h5>
              <span>{item.mimeType}</span>
            </Col>
            <Col span={8}>
              <h5>DOCUMENT TYPE</h5>
              <span>{docType}</span>
            </Col>
          </Row>
        </Card>
        <div style={{ marginTop: 20 }}>
          <Radio.Group
            onChange={this.handleModeChange}
            value={mode}
            style={{ marginBottom: 8 }}
          >
            <Radio.Button value='top'>Horizontal</Radio.Button>
            <Radio.Button value='left'>Vertical</Radio.Button>
          </Radio.Group>
          <Tabs defaultActiveKey='1' tabPosition={mode}>
            <TabPane tab={'Procesess ' + procs.count()} key='1'>
              <Spin spinning={procLoading}>
                <ProcessTab
                  processes={procs}
                  services={processServices}
                  loading={procLoading}
                  workflows={workflows}
                />
              </Spin>
            </TabPane>
          </Tabs>
        </div>
      </div>
    )
  }
}

export default DocumentView
