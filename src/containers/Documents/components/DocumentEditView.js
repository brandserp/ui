import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Header,
  Segment,
  Grid,
  Icon,
  Button,
  Message,
  Input
} from 'semantic-ui-react'

import { ResourceEditView } from 'components/Views'

class DocumentEditView extends ResourceEditView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      form: {
        id: props.item ? props.item.id : '',
        documentTypeId: props.item ? props.item.documentTypeId : '',
        filename: props.item ? props.item.filename : ''
      }
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      this.setState({
        form: {
          id: nextProps.item.id,
          documentTypeId: nextProps.item.documentTypeId || '',
          filename: nextProps.item.filename
        }
      })
    }
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newCollection = {
      ...form
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const { filename, documentTypeId } = this.state.form
    const { resourceName, saving, dependencies, item } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const ops = dependencies.documentTypes
      .filter(f => f.properties.fileTypes.includes(item.mimeType))
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    const secAction = [
      {
        content: 'View File',
        icon: 'external',
        onAction: () => window.open(item.url)
      }
    ]
    const isValid = documentTypeId !== '' && ops.length !== 0

    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Document - {`${item.id}`}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Segment loading={saving}>
              <Form size='small'>
                <Form.Group>
                  <Form.Field width={6}>
                    <label>File Name</label>
                    <Input
                      label={`.${item.extension}`}
                      labelPosition='right'
                      name='filename'
                      placeholder='File name'
                      readOnly
                      value={filename}
                      onChange={this.updateForm}
                    />
                  </Form.Field>
                  <Form.Input
                    width={5}
                    name='mimeType'
                    label='MIME Type'
                    readOnly
                    value={item.mimeType}
                  />
                  <Form.Dropdown
                    width={5}
                    label='Document Type'
                    name='documentTypeId'
                    placeholder='Choose document type'
                    selection
                    search
                    required
                    options={ops}
                    value={documentTypeId}
                    onChange={this.updateForm}
                    disabled={ops.length === 0}
                  />
                </Form.Group>
              </Form>
              {ops.length === 0 ? (
                <Message error>
                  <Icon name='warning circle' />
                  This file type does not have supporting document types. Saving
                  is disabled.
                </Message>
              ) : null}
            </Segment>
            <Button
              floated='right'
              disabled={!isValid}
              positive
              content='Save'
              onClick={() => this.handleSave()}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default DocumentEditView
