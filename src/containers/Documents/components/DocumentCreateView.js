import React from 'react'
import PropTypes from 'prop-types'
import Uppy from '@uppy/core'
import { Dashboard } from '@uppy/react'
import S3Uploader from '@uppy/aws-s3'
import { Card, Row, Col, Icon, Button, Select } from 'antd'
import api from 'middleware/api'
import { ResourceCreateView } from 'components/Views'

const Option = Select.Option

class DocumentCreateView extends ResourceCreateView {
  constructor (props) {
    super(props)
    this.uppy = new Uppy({
      debug: true
    })
      .use(S3Uploader, {
        getUploadParameters (file) {
          // Send a request to our PHP signing endpoint.
          return api
            .post('/s3-sign', {
              filename: encodeURIComponent(file.name),
              contentType: file.type
            })
            .then(({ data }) => {
              // Return an object in the correct shape.
              return {
                method: 'PUT',
                url: data.url,
                headers: {
                  'content-type': file.type,
                  'content-disposition': `inline`
                },
                fields: []
              }
            })
        }
      })
      .on('file-added', file => {
        file.name = file.name.replace(/[\s\uFEFF\xA0]/g, '_')
        file.meta.name = file.name
        this.addFile(file)
      })
      .on('file-removed', file => {
        this.removeFile(file)
      })
      .on('upload-success', (file, data) => {
        const path = data.location.split('.com')
        this.updateFile(null, { name: 'path', value: path[1], id: file.id })
      })
  }

  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    files: []
  }

  componentWillUnmount () {
    this.uppy.close()
  }

  updateFile = (e, { name, value, id }) => {
    let data = this.state.files
    const idx = data.findIndex(f => f.id === id)
    let file = data[idx]
    file[name] = value
    data[idx] = file
    this.setState({
      files: data
    })
  }

  addFile = info => {
    let data = this.state.files
    const idx = data.findIndex(f => f.id === info.id)
    if (idx === -1) {
      data.push({ path: '', info, documentTypeId: '', id: info.id })
      this.setState({
        files: data
      })
    }
  }

  removeFile = file => {
    let data = this.state.files
    const idx = data.findIndex(f => f.id === file.id)
    data.splice(idx, 1)
    this.setState({
      files: data
    })
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { files } = this.state
    const newCollection = {
      files
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderTypes = () => {
    const { files } = this.state
    const { dependencies } = this.props

    let list = files.map((v, k) => {
      let ops = dependencies.documentTypes
        .filter(f => f.properties.fileTypes.includes(v.info.type))
        .map(r => ({
          key: r.id,
          text: r.name,
          value: r.id
        }))
        .toList()
        .toJS()
      return (
        <div>
          <label style={{ fontWeight: 'bold' }}>{v.info.name}</label>
          <Select
            style={{ width: 200 }}
            placeholder={
              ops.length
                ? 'Choose document type'
                : 'No document types for this file type'
            }
            optionFilterProp='children'
            onChange={e =>
              this.updateFile(e, { name: 'documentTypeId', value: e, id: v.id })
            }
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {ops.map((r, k) => (
              <Option key={r.id} value={r.value}>
                {r.text}
              </Option>
            ))}
          </Select>
        </div>
      )
    })
    return list
  }

  renderContent = () => {
    const { files } = this.state
    const { resourceName } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = files.length > 0
    const uppy = this.uppy
    const dashProps = {
      uppy,
      height: 400,
      thumbnailWidth: 180,
      showProgressDetails: true,
      browserBackButtonClose: false,
      proudlyDisplayPoweredByUppy: false
    }
    return (
      <div>
        <h1>Create Document(s)</h1>
        <Card style={{ border: '1px solid #F65E7D' }}>
          <Row gutter={24}>
            <Col span={2}>
              <Icon style={{ fontSize: 40 }} type='exclamation-circle' />
            </Col>
            <Col span={22}>
              <ul>
                <li>
                  Document types that support each file type can be set after
                  adding files,
                </li>
                <li>
                  Files uploaded without a document type can be modified later
                  on "Pending Files" page
                </li>
              </ul>
            </Col>
          </Row>
        </Card>
        <div style={{ marginTop: 20 }}>
          <Row gutter={24}>
            <Col span={15}>
              <Dashboard {...dashProps} />
            </Col>
            <Col span={9}>{files.length > 0 ? this.renderTypes() : null}</Col>
          </Row>
        </div>
        <Button
          type='primary'
          disabled={!isValid}
          style={{ float: 'right', marginBottom: 10 }}
          onClick={() => this.handleSave()}
        >
          Save
        </Button>
      </div>
    )
  }
}

export default DocumentCreateView
