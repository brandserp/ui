import React from 'react'
// import { Button, Segment, Header, Icon } from 'semantic-ui-react'
import { Empty, Button } from 'antd'

const EmptyFileTypes = ({ message, add, action }) => (
  <Empty
    description={
      <span style={{ fontSize: 16, fontWeight: 'bold' }}>{message}</span>
    }
  >
    {action && add ? (
      <Button onClick={() => add()} type='primary'>
        {action}
      </Button>
    ) : null}
  </Empty>
)

export default EmptyFileTypes
