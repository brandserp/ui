import { connect } from 'react-redux'

import CollectionEditView from './components/CollectionEditView'
import { modelConfig } from 'models/collections'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.collections.getOne(state, { id: match.params.id }),
    dependencies: {
      documentTypes: state.documentTypes.get('list'),
      documents: state.documents.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'container/collections',
    loading: state.loading.effects.collections.fetch || false,
    drawerFetch: state.loading.effects.documents.fetchAll,
    saving:
      state.loading.effects.collections.update ||
      state.loading.effects.collections.attach ||
      state.loading.effects.collections.detach ||
      false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    prefetch: [() => dispatch.collections.fetch({ id })],
    fetchDocs: () => dispatch.documents.fetchAll(),
    saveAction: collection => dispatch.collections.update(collection),
    addAction: docs => dispatch.collections.attach({ id, docs }),
    removeAction: docs => {
      dispatch.collections.detach({ id, docs })
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionEditView)
