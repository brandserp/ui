import React, { Component } from 'react'
import propTypes from 'prop-types'
import {
  Grid,
  Segment,
  Step,
  Label,
  Table,
  Divider,
  Button
} from 'semantic-ui-react'
import moment from 'moment'

import { ContentLoader } from 'components/Loader'

class CollectionsView extends Component {
  static propTypes = {
    collections: propTypes.object.isRequired,
    loading: propTypes.bool,
    doFetchCollections: propTypes.func.isRequired,
    navigationViewController: propTypes.object
  }

  static contextTypes = {
    router: propTypes.object
  }

  state = {
    addNew: '',
    editMode: false,
    editTarget: null,
    editValue: '',
    removeMode: false,
    removeTarget: null
  }

  componentWillMount () {
    this.props.doFetchCollections()
  }

  componentDidMount () {
    const { navigationViewController } = this.props
  }

  renderRow = (v, k) => {
    const { workflows } = this.props
    return (
      <Table.Row
        onClick={() => this.context.router.history.push(`/collections/${v.id}`)}
        style={{ cursor: 'pointer' }}
        key={k}
      >
        <Table.Cell collapsing>{v.id}</Table.Cell>
        <Table.Cell>{v.getIn(['workflow', 'name'])}</Table.Cell>
        <Table.Cell>{v.processInstanceId}</Table.Cell>
        <Table.Cell collapsing>{v.createdAt}</Table.Cell>
        <Table.Cell>{v.updatedAt}</Table.Cell>
        <Table.Cell collapsing textAlign='right'>
          <Button
            onClick={() =>
              this.context.router.history.push(`/collections/edit/${v.id}`)
            }
            icon='pencil'
            circular
            compact
            size='tiny'
          />
        </Table.Cell>
      </Table.Row>
    )
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      ...this.state,
      ...data
    })
  }

  renderTable = () => {
    const { collections } = this.props
    return (
      <Table selectable celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Workflow Name</Table.HeaderCell>
            <Table.HeaderCell>Process ID</Table.HeaderCell>
            <Table.HeaderCell>Created</Table.HeaderCell>
            <Table.HeaderCell>Updated</Table.HeaderCell>
            <Table.HeaderCell textAlign='right' />
          </Table.Row>
        </Table.Header>
        <Table.Body>{collections.toList().map(this.renderRow)}</Table.Body>
      </Table>
    )
  }

  renderLoading = () => {
    return <ContentLoader />
  }

  render () {
    const { collections, loading } = this.props
    return (
      <div>
        <Grid columns={1}>
          <Grid.Row>
            <Grid.Column>
              <Step.Group size='tiny'>
                <Step title='Documents' />
                <Step active title='Collections' />
              </Step.Group>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              {!loading ? this.renderTable() : this.renderLoading()}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

export default CollectionsView
