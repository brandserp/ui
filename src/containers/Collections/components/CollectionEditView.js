import React from 'react'
import PropTypes from 'prop-types'
import {
  Header,
  Segment,
  Table,
  Icon,
  Menu,
  Dropdown,
  Form,
  Grid
} from 'semantic-ui-react'
import { Drawer } from 'antd'
import { getFontAwesomeIconFromMIME } from 'utils/fileIcons'
import { Link } from 'react-router-dom'

import { ResourceEditView } from 'components/Views'
import EmptyState from './EmptyState'
import DocumentPicker from './DocumentPicker'

class CollectionEditView extends ResourceEditView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  constructor (props) {
    super(props)
    const item = props.item ? props.item.toJS() : props.item
    this.state = {
      isDrawerOpen: false,
      editMode: false,
      form: {
        id: item ? item.id : '',
        name: item ? item.name : ''
      }
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      const item = nextProps.item.toJS()
      this.setState({
        form: {
          id: item.id,
          name: item.name || ''
        }
      })
    }
  }

  openDrawer = pane =>
    this.setState({
      isDrawerOpen: pane
    })

  closeDrawer = () =>
    this.setState({
      isDrawerOpen: false
    })

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  toggleEdit = () => this.setState({ editMode: !this.state.editMode })

  addDocuments = docs => {
    const { addAction, item } = this.props
    addAction({ media: docs }, item.id)
    this.closeDrawer()
  }

  removeDocument = id => {
    const { removeAction, item } = this.props
    const docs = {
      media: [id]
    }
    removeAction(docs, item.id)
  }

  handleSave = () => {
    const { saveAction } = this.props
    const { form } = this.state
    const newCollection = {
      ...form
    }
    saveAction(newCollection)
    this.toggleEdit()
  }

  renderDocs = () => {
    const {
      item,
      dependencies: { documentTypes, documents }
    } = this.props
    const docs = documents
      .filter(d => item.media.includes(d.id))
      .toList()
      .toJS()
      .map((v, k) => (
        <Table.Row key={v.id}>
          <Table.Cell>
            <Icon
              size='big'
              name={getFontAwesomeIconFromMIME(v.mimeType)}
              style={{ marginRight: 16 }}
            />
            {documentTypes.getIn([`${v.documentTypeId}`, 'name'])}
          </Table.Cell>
          <Table.Cell>
            <Link to={`/documents/${v.id}`}>
              {`${v.filename}.${v.extension}`}
            </Link>
          </Table.Cell>
          <Table.Cell textAlign='right'>
            <Dropdown
              size='large'
              icon={{
                name: 'ellipsis horizontal',
                link: true,
                circular: true,
                inverted: true,
                color: 'grey'
              }}
              className='icon'
              direction='left'
            >
              <Dropdown.Menu className='large'>
                <Dropdown.Item text='Start Workflow' icon='play' />
                <Dropdown.Item
                  text='Remove From Collection'
                  icon={{ name: 'remove circle', color: 'red' }}
                  onClick={() => this.removeDocument(v.id)}
                />
              </Dropdown.Menu>
            </Dropdown>
          </Table.Cell>
        </Table.Row>
      ))

    return (
      <Table striped verticalAlign='middle' size='small' padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Document Type</Table.HeaderCell>
            <Table.HeaderCell colSpan='2'>File</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{docs}</Table.Body>
      </Table>
    )
  }

  renderContent = () => {
    const { form, isDrawerOpen, editMode } = this.state
    const { name } = form
    const { resourceName, saving, dependencies, item, drawerFetch } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]

    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Edit Collection - {`${item.id}`}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Drawer
              width={460}
              onClose={this.closeDrawer}
              visible={isDrawerOpen}
              title='Add Documents'
            >
              {isDrawerOpen === 'documents' ? (
                <DocumentPicker
                  className='document-picker'
                  fetching={drawerFetch}
                  fetchAction={this.props.fetchDocs}
                  documents={dependencies.documents.filterNot(v =>
                    item.media.includes(v.id)
                  )}
                  documentTypes={dependencies.documentTypes}
                  onComplete={this.addDocuments}
                />
              ) : null}
            </Drawer>
            <Menu size='small' borderless attached='top'>
              <Menu.Item>
                <Header
                  color='blue'
                  icon='list alternate outline'
                  content='Details'
                />
              </Menu.Item>
              <Menu.Menu position='right'>
                <Menu.Item
                  icon={editMode ? 'ban' : 'edit'}
                  content='Edit'
                  active={editMode}
                  color='red'
                  onClick={() => this.toggleEdit()}
                />
              </Menu.Menu>
            </Menu>
            <Segment attached='bottom' size='small'>
              {editMode ? (
                <Form size='small' onSubmit={() => this.handleSave()}>
                  <Form.Group>
                    <Form.Input
                      inline
                      label='Collection Name'
                      name='name'
                      value={name}
                      onChange={this.updateForm}
                    />
                    <Form.Button
                      size='small'
                      type='submit'
                      content='Save'
                      positive
                    />
                  </Form.Group>
                </Form>
              ) : (
                <Header textAlign='left'>
                  <Header sub>Collection Name</Header>
                  {name || 'Untitled'}
                </Header>
              )}
            </Segment>

            <Menu size='small' borderless attached='top'>
              <Menu.Item>
                <Header color='blue' icon='file outline' content='Documents' />
              </Menu.Item>
              <Menu.Menu position='right'>
                <Menu.Item
                  icon='add circle'
                  content='Add Documents'
                  onClick={() => this.openDrawer('documents')}
                />
              </Menu.Menu>
            </Menu>

            <Segment size='small' attached='bottom' loading={saving}>
              {item.media && item.media.count && item.media.count() ? (
                this.renderDocs()
              ) : (
                <EmptyState add={() => this.openDrawer('documents')} />
              )}
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default CollectionEditView
