import { connect } from 'react-redux'
import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/collections'
import CollectionListItem from './components/CollectionListItem'
import EditContainer from './Edit'
import store from 'store'

const mapStateToProps = (state, { location, match }) => {
  const { resourceName, resourceList } = modelConfig
  const authUser = store.select.auth.getAuthUser(state)
  const viewId = new URLSearchParams(location.search).get('view')
  const defaultView = new URLSearchParams(location.search).get('default')
  const view = viewId ? store.select.views.getOne(state, { id: viewId }) : null

  return {
    items: store.select.collections.getItems(state),
    params: store.select.collections.getParams(state),
    resourceName,
    resourceList,
    authUser,
    view,
    viewId,
    customViews: true,
    viewType: 'collections',
    defaultView,
    slideOutWidth: 968,
    itemRenderer: CollectionListItem,
    detailView: EditContainer,
    loading:
      state.loading.effects.collections.fetchAll ||
      state.loading.effects.workflows.fetchAll ||
      state.loading.effects.views.fetchByType
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    primaryAction: [
      {
        content: 'Create Collection',
        onClick: () => dispatch.collections.create()
      }
    ],
    prefetch: [
      () => dispatch.workflows.fetchAll(),
      () => dispatch.views.fetchByType('collections')
    ],
    refetch: () => dispatch.collections.fetchAll(),
    changeParams: filters => dispatch.collections.changeParams(filters),
    resetParams: () => dispatch.collections.resetParams(),
    removeAction: id => dispatch.collections.remove(id),
    saveView: view => dispatch.views.create(view),
    updateView: id => dispatch.views.update(id),
    renameView: view => dispatch.views.rename(view),
    deleteView: id => dispatch.views.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
