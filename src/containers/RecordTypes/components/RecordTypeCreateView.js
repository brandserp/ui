import React from 'react'
import PropTypes from 'prop-types'
import { Header } from 'semantic-ui-react'
import {
  Button,
  Form,
  Input,
  InputNumber,
  Row,
  Col,
  Layout,
  Card,
  Select,
  Menu,
  Empty,
  Switch
} from 'antd'
import _ from 'lodash'
import shortid from 'shortid'
import { fromJS, Record } from 'immutable'

import { ResourceCreateView } from 'components/Views'
import {
  Form as JSONForm,
  ObjectFieldBuilderTemplate,
  BuilderFieldTemplate
} from 'components/JSONschema'
import DetailsForm from './DetailsForm'
import FormBuilder from 'components/FormBuilder'

const InitData = Record({
  schema: fromJS({
    type: 'object',
    required: [],
    properties: {}
  }),
  uiSchema: fromJS({
    'ui:order': []
  }),
  meta: fromJS({
    listFields: fromJS([]),
    relationships: fromJS({
      hasOne: [],
      hasMany: []
    })
  }),
  selectedField: null
})

class RecordTypeCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    toggleView: 'fields',
    form: {
      name: {
        value: ''
      },
      description: {
        value: ''
      }
    },
    data: new InitData()
  }

  setImmState = fn => {
    this.setState(({ data }) => ({
      data: fn(data)
    }))
  }

  updateView = key => this.setState({ toggleView: key })

  handleFormChange = changedFields => {
    this.setState(({ form }) => ({
      form: { ...form, ...changedFields }
    }))
  }

  componentWillMount () {
    const { prefetch } = this.props
    if (prefetch && prefetch.length) {
      prefetch.forEach(p => p())
    }
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form, data } = this.state
    const { schema, meta, uiSchema } = data.toJS()
    const newRecordType = {
      ..._.mapValues(form, v => v.value),
      schema: JSON.stringify(schema),
      uiSchema: JSON.stringify(uiSchema),
      meta: JSON.stringify(meta)
    }
    saveAction(newRecordType).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const { data, toggleView, form } = this.state
    const { dependencies } = this.props

    const tabList = [
      { key: 'fields', tab: 'Fields' },
      { key: 'options', tab: 'Options' }
    ]

    return (
      <Layout.Content>
        <Header as='h2' dividing>
          Create Record Type
        </Header>
        <Row>
          <Col>
            <Card
              title='Record Details'
              size='small'
              style={{ marginBottom: '1.5rem' }}
            >
              <DetailsForm {...form} onChange={this.handleFormChange} />
            </Card>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Card
              tabList={tabList}
              onTabChange={k => this.updateView(k)}
              title='Form Builder'
              size='small'
              activeTabKey={toggleView}
            >
              <FormBuilder
                dependencies={dependencies}
                data={data}
                setImmState={this.setImmState}
                view={toggleView}
              />
            </Card>
          </Col>
        </Row>
        <Row justify='end' type='flex'>
          <Col span={2} style={{ paddingTop: '2em' }}>
            <Button block type='primary' onClick={() => this.handleSave()}>
              Save
            </Button>
          </Col>
        </Row>
      </Layout.Content>
    )
  }
}

export default RecordTypeCreateView
