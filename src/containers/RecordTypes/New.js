import { connect } from 'react-redux'

import RecordTypeCreateView from './components/RecordTypeCreateView'
import { modelConfig } from 'models/recordTypes'

import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    dependencies: {
      recordTypes: store.select.recordTypes.getList(state)
    },
    resourceName,
    loading: state.loading.effects.recordTypes.fetchAll || false,
    saving: state.loading.effects.recordTypes.create || false,
    slideoutWidth: '90%'
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
    ],
    saveAction: recordType => dispatch.recordTypes.create(recordType)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordTypeCreateView)
