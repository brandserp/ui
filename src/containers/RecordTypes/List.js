import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/recordTypes'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RecordTypesListItem'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.recordTypes.getItems(state),
    params: state.recordTypes.get('params'),
    resourceName,
    resourceList,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'container/settings',
    loading: state.loading.effects.recordTypes.fetchAll || false,
    slideoutWidth: 1280
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.recordTypes.fetchAll()],
    refetch: ops => dispatch.recordTypes.fetchAll(ops),
    changeParams: filters => dispatch.recordTypes.changeParams(filters),
    resetParams: () => dispatch.recordTypes.resetParams(),
    removeAction: id => dispatch.recordTypes.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
