import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { ResourceCreateView } from 'components/Views'
import { Form as JSONForm } from 'components/JSONschema'
import { Select, Empty, Spin, Divider } from 'antd'
import ErrorList from 'components/JSONschema/ErrorList'

const Option = Select.Option

class RecordCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    form: {
      recordTypeId: ''
    },
    data: ''
  }

  componentDidMount () {
    const { navigationViewController } = this.props
  }

  componentWillUpdate (nextProps, nextState) {
    const { recordTypeId } = nextState.form
    if (recordTypeId && recordTypeId !== this.state.form.recordTypeId) {
      const recordProps = this.props.dependencies.recordTypes.get(recordTypeId)
      const schema = JSON.parse(recordProps.schema)

      const data = _.mapValues(schema.properties, () => '')
      this.setState({
        data
      })
    }
  }

  updateForm = e => {
    const data = {}
    data['recordTypeId'] = e
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateFormData = ({ formData }) => {
    let data = formData
    Object.keys(data).forEach(
      key => data[key] === undefined && delete data[key]
    )
    this.setState({
      data
    })
  }

  handleSave = ({ formData }) => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newRecord = {
      ...form,
      data: JSON.stringify(formData)
    }
    saveAction(newRecord).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderSchemaBuilder = () => {
    const { data, form } = this.state
    const { recordTypes } = this.props.dependencies
    const { saving } = this.props
    const recordType = recordTypes.get(form.recordTypeId).toJS()
    const viewerProps = {
      formData: data,
      schema: JSON.parse(recordType.schema),
      uiSchema: JSON.parse(recordType.uiSchema),
      noHtml5Validate: true,
      onSubmit: this.handleSave,
      showErrorList: false
    }
    const labelStyle = {
      margin: 0,
      top: 0,
      left: 0,
      padding: '.75em 1em',
      width: 'auto',
      marginTop: 0,
      borderRadius: '.21428571rem 0 .28571429rem 0',
      fontSize: '.85714286rem',
      display: 'inline-block',
      lineHeight: 1,
      verticalAlign: 'baseline',
      backgroundColor: '#e8e8e8',
      backgroundImage: 'none',
      padding: '.5833em .833em',
      color: 'rgba(0,0,0,.6)',
      textTransform: 'none',
      fontWeight: 700,
      border: '0 solid transparent',
      borderRadius: '.28571429rem',
      transition: 'background .1s ease'
    }

    return (
      <Spin spinning={saving} delay={500}>
        <div style={{ border: '1px solid #D9D9D9', marginTop: 20 }}>
          <label style={labelStyle}>Record Data</label>
          <JSONForm {...viewerProps} />
        </div>
      </Spin>
    )
  }

  renderContent = () => {
    const { recordTypeId } = this.state.form
    const { data } = this.state
    const { resourceName, resourceForm, saving, dependencies } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = recordTypeId !== ''
    const typeOps = dependencies.recordTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    return (
      <div>
        <h1>Create Record</h1>
        <Divider />
        <Spin spinning={saving} delay={500}>
          <div
            style={{
              width: '100%',
              minHeight: 95,
              border: '1px solid #e8e8e8',
              padding: 10
            }}
          >
            <h5 style={{ fontWeight: 'bold' }}>Record Type</h5>
            <Select
              placeholder='Choose a record type'
              style={{ width: '100%' }}
              onChange={this.updateForm}
            >
              {typeOps.map((v, k) => (
                <Option value={v.value}>{v.text}</Option>
              ))}
            </Select>
          </div>
        </Spin>

        {recordTypeId && data ? (
          this.renderSchemaBuilder()
        ) : (
          <Empty
            style={{ marginTop: 20 }}
            description={
              <p>
                <span style={{ fontWeight: 'bold' }}>
                  No Record Type Selected,{' '}
                </span>
                select a record type to create...
              </p>
            }
          />
        )}
      </div>
    )
  }
}

export default RecordCreateView
