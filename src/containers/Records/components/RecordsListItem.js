import React from 'react'
import { Header, Grid } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const RecordsListItem = ({ item, index, dependencies }) => {
  let rt = dependencies.recordTypes[`${item.recordTypeId}`]
  const { listFields } = rt.meta
  const customField = listFields.length ? listFields[0] : null
  return (
    <Grid verticalAlign='middle'>
      <Grid.Column textAlign='left'>
        <Header as='h4' sub color='blue'>
          {rt.name}
        </Header>
        <Header as='h5'>
          <Ellipsis text={item.data[customField]} basedOn='letters' trimRight />
        </Header>
      </Grid.Column>
    </Grid>
  )
}

export default RecordsListItem
