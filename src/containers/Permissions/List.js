import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/permissions'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/PermissionsListItem'
import store from 'store'

const mapStateToProps = state => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.permissions.getItems(state),
    params: state.permissions.get('params'),
    resourceName,
    resourceList,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'container/settings',
    loading: state.loading.effects.permissions.fetchAll || false
  }
}

const mapDispatchToProps = dispatch => {
  return {
    prefetch: [() => dispatch.permissions.fetchAll()],
    refetch: () => dispatch.permissions.fetchAll(),
    changeParams: filters => dispatch.permissions.changeParams(filters),
    removeAction: id => dispatch.permissions.remove(id),
    resetParams: () => dispatch.permissions.resetParams()
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
