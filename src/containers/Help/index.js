import React, { Component } from 'react'
import HelpHome from './components/helpHome'
import { Drawer } from 'antd'
import TopicContent from './components/topicContent'

class HelpDrawerContent extends Component {
  state = { activeTopic: null }

  handleTopicChange = topic => {
    this.setState({ activeTopic: topic })
  }

  handleBackToMainHelpPage = () => {
    this.setState({ activeTopic: null })
  }

  render () {
    const { handleOnClose, isVisible } = this.props
    const { activeTopic } = this.state
    return (
      <Drawer
        title={activeTopic || 'Need help on...'}
        placement='right'
        closable
        onClose={() => handleOnClose(this.handleBackToMainHelpPage)}
        visible={isVisible}
        width={320}
        destroyOnClose
      >
        {activeTopic ? (
          <TopicContent
            topic={activeTopic}
            goBack={this.handleBackToMainHelpPage}
          />
        ) : (
          <HelpHome handleTopicChange={this.handleTopicChange} />
        )}
      </Drawer>
    )
  }
}

export default HelpDrawerContent
