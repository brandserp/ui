import React, { Component } from 'react'
import { List, Avatar } from 'antd'

const data = [
  {
    title: 'Tasks'
  },
  {
    title: 'Documents'
  },
  {
    title: 'Records'
  },
  {
    title: 'Collections'
  },
  {
    title: 'Settings'
  }
]

class HelpHome extends Component {
  render () {
    return (
      <List
        itemLayout='horizontal'
        dataSource={data}
        renderItem={item => (
          <List.Item>
            <List.Item.Meta
              avatar={<Avatar icon='question' />}
              title={item.title}
              description={
                <a onClick={() => this.props.handleTopicChange(item.title)}>
                  Read More
                </a>
              }
            />
          </List.Item>
        )}
      />
    )
  }
}

export default HelpHome
