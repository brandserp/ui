import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Menu, Icon, Layout, Skeleton } from 'antd'
import { ucFirst } from 'utils/render'
import { connect } from 'react-redux'
import store from 'store'

class SearchListMenu extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const {
      defaultViews,
      customViews,
      location,
      match,
      name,
      loading
    } = this.props
    const matchPath = location.search
      ? location.pathname + location.search
      : location.pathname
    const customViewList = customViews.map((v, k) => {
      const key = `${match.path}?view=${v.get('id')}`
      return (
        <Menu.Item key={key} onClick={() => this.goTo(key)}>
          {v.get('name')}
        </Menu.Item>
      )
    })
    return (
      <Fragment>
        <Layout.Content>
          <div
            className='menu-wrapper'
            style={{ height: '100%', paddingTop: '24px' }}
          >
            <Menu mode='inline' theme='dark' selectedKeys={[matchPath]}>
              <Menu.ItemGroup key='default-views' title='Default Views'>
                {defaultViews.map((v, k) => (
                  <Menu.Item key={v.key} onClick={() => this.goTo(v.key)}>
                    {v.title}
                  </Menu.Item>
                ))}
              </Menu.ItemGroup>
            </Menu>
            <Skeleton
              loading={loading}
              title={false}
              active
              paragraph={{ rows: 4 }}
            >
              <Menu mode='inline' theme='dark' selectedKeys={[matchPath]}>
                <Menu.ItemGroup key='custom-views' title='My Saved Views'>
                  {customViewList}
                </Menu.ItemGroup>
              </Menu>
            </Skeleton>
          </div>
        </Layout.Content>
      </Fragment>
    )
  }
}

const mapStateToProps = (state, props) => ({
  ...props,
  customViews: store.select.views.getViewsByType(
    state,
    props.name.toLowerCase()
  ),
  loading: state.loading.models.views
})

export default connect(
  mapStateToProps,
  {}
)(SearchListMenu)
