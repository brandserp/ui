import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Menu, Layout } from 'antd'

class AccountingMenu extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const { location } = this.props
    const matchPath = location.pathname
    const basePath = '/accounting'
    return (
      <Fragment>
        <Layout.Header>Accounting</Layout.Header>
        <Layout.Content>
          <div
            className='menu-wrapper'
            style={{ height: 'calc(100vh - 64px)' }}
          >
            <Menu mode='inline' theme='dark' selectedKeys={[matchPath]}>
              <Menu.ItemGroup
                key='Record'
                title='Record'
              >
                <Menu.Item key={`${basePath}/transaction/new`} onClick={() => this.goTo(`${basePath}/transaction/new`)}>
                  General Transaction
                </Menu.Item>
                <Menu.Item key={`${basePath}/receipt/new`} onClick={() => this.goTo(`${basePath}/receipt/new`)}>
                  Simple Reciept
                </Menu.Item>
                <Menu.Item
                  key={`${basePath}/statements`}
                  onClick={() => this.goTo(`${basePath}/statements`)}
                >
                  Statement Balances
                </Menu.Item>
                <Menu.Item
                  key={`${basePath}/rent-charges`}
                  onClick={() => this.goTo(`${basePath}/rent-charges`)}
                >
                  Rent Charges
                </Menu.Item>
              </Menu.ItemGroup>
              <Menu.ItemGroup key='reconcile' title='Reconcile'>
                <Menu.Item
                  key={`${basePath}/reconcile/entries`}
                  onClick={() => this.goTo(`${basePath}/reconcile/entries`)}
                >
                  Reconcile Entries
                </Menu.Item>
                <Menu.Item
                  key={`${basePath}/reconcile/statements`}
                  onClick={() => this.goTo(`${basePath}/reconcile/statements`)}
                >
                  Reconcile Statements
                </Menu.Item>
              </Menu.ItemGroup>
            </Menu>
          </div>
        </Layout.Content>
      </Fragment>
    )
  }
}

export default AccountingMenu
