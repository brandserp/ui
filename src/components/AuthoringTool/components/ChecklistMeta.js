import React from 'react'
import { Card } from 'antd'
import styles from './ChecklistMeta.module.scss'
import EmptyState from './EmptyState.svg'

const ChecklistMeta = () => {
  return (
    <Card className={styles.default}>
      <Card.Header>
        <Card.Title className='h5'>Step Builder</Card.Title>
      </Card.Header>
      <Card.Body>
        <div className='empty'>
          <img src={EmptyState} />
          <p>Click on a step or create a new one to begin building.</p>
        </div>
      </Card.Body>
    </Card>
  )
}

export default ChecklistMeta
