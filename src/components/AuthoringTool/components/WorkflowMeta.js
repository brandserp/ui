import React from 'react'
import { Empty, Button } from 'antd'
import styles from './WorkflowMeta.module.scss'
import EmptyState from './EmptyState.svg'

const WorkflowMeta = () => {
  return (
    <div>
      <Empty description={<span>Select an existing step or...</span>}>
        <Button type='primary'>Create Now</Button>
      </Empty>
    </div>
  )
}

export default WorkflowMeta
