import React from 'react'
import { Tile } from 'components/Atoms'
import cx from 'classnames'
import styles from './StepMarker.module.scss'
// and list component

const StepMarker = props => {
  const { type, title, index, className, selected, ...restProps } = props

  return (
    <Tile
      {...restProps}
      centered
      className={cx(styles.default, className, {
        selected: selected
      })}
    >
      <Tile.Icon>{index || <span>&bull;</span>}</Tile.Icon>
      <Tile.Action>test</Tile.Action>
      <Tile.Content>
        <Tile.Title>{title || 'Untitled Step'}</Tile.Title>
      </Tile.Content>
    </Tile>
  )
}

export default StepMarker
