import ArrayField from 'react-jsonschema-form/lib/components/fields/ArrayField'
import shortid from 'shortid'

import {
  getDefaultFormState,
  isFixedItems,
  allowAdditionalItems,
  getDefaultRegistry
} from 'react-jsonschema-form/lib/utils'

class StepsDetailsArrayField extends ArrayField {
  onAddClick = (event, type = null) => {
    if (event) {
      event.preventDefault()
    }
    const { schema, formData, registry = getDefaultRegistry() } = this.props
    const { definitions } = registry
    let itemSchema = schema.items
    if (isFixedItems(schema) && allowAdditionalItems(schema)) {
      itemSchema = schema.additionalItems
    }
    const newItem = getDefaultFormState(
      itemSchema,
      { id: shortid.generate(), type },
      definitions
    )

    this.props.onChange([...formData, newItem])
  }
}

export default StepsDetailsArrayField
