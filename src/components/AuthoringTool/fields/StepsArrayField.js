import ArrayField from 'react-jsonschema-form/lib/components/fields/ArrayField'
import shortid from 'shortid'

import {
  getDefaultFormState,
  isFixedItems,
  allowAdditionalItems,
  getDefaultRegistry
} from 'react-jsonschema-form/lib/utils'

class StepsArrayField extends ArrayField {
  onAddClick = (startIdx = null, type = 'UserTask', duplicate = false) => {
    const { schema, formData, registry = getDefaultRegistry() } = this.props
    const { definitions } = registry
    let itemSchema = schema.items
    if (isFixedItems(schema) && allowAdditionalItems(schema)) {
      itemSchema = schema.additionalItems
    }
    const newItem = duplicate
      ? {
        ...formData[startIdx],
        id: `${type}_${shortid.generate()}`,
        focused: true
      }
      : getDefaultFormState(
        itemSchema,
        {
          id: `${type}_${shortid.generate()}`,
          type,
          title: '',
          description: '',
          focused: true
        },
        definitions
      )

    if (startIdx !== null) {
      const newItems = [...formData]
      const newIdx = startIdx + 1
      newItems.splice(newIdx, 0, newItem)
      this.props.onChange([...newItems])
    } else {
      const newItems = [...formData]
      const newIdx = newItems.length - 1
      newItems.splice(newIdx, 0, newItem)
      this.props.onChange([...newItems])
    }
  }
}

export default StepsArrayField
