import React from 'react'
import ObjectField from 'react-jsonschema-form/lib/components/fields/ObjectField'
import FormBuilder from 'components/FormBuilder'
import _ from 'lodash'
import { fromJS, Record } from 'immutable'

import api from 'middleware/api'

import {
  orderProperties,
  retrieveSchema,
  getDefaultRegistry,
  getUiOptions,
  ADDITIONAL_PROPERTY_FLAG,
  getDefaultFormState
} from 'react-jsonschema-form/lib/utils'

import {
  Button,
  Form,
  Input,
  InputNumber,
  Layout,
  Card,
  Select,
  Menu,
  Empty,
  Switch,
  Row,
  Col
} from 'antd'

function DefaultObjectFieldTemplate (props) {
  const canExpand = function canExpand () {
    const { formData, schema, uiSchema } = props
    if (!schema.additionalProperties) {
      return false
    }
    const { expandable } = getUiOptions(uiSchema)
    if (expandable === false) {
      return expandable
    }
    // if ui:options.expandable was not explicitly set to false, we can add
    // another property if we have not exceeded maxProperties yet
    if (schema.maxProperties !== undefined) {
      return Object.keys(formData).length < schema.maxProperties
    }
    return true
  }

  const { TitleField, DescriptionField } = props
  return (
    <fieldset id={props.idSchema.$id}>
      {(props.uiSchema['ui:title'] || props.title) && (
        <TitleField
          id={`${props.idSchema.$id}__title`}
          title={props.title || props.uiSchema['ui:title']}
          required={props.required}
          formContext={props.formContext}
        />
      )}
      {props.description && (
        <DescriptionField
          id={`${props.idSchema.$id}__description`}
          description={props.description}
          formContext={props.formContext}
        />
      )}
      {props.properties.map(prop => prop.content)}
    </fieldset>
  )
}

class JsonFormField extends ObjectField {
  constructor (props) {
    super(props)
    const { formData, formContext } = this.props

    const InitData = Record({
      schema: fromJS(formData.schema),
      uiSchema: fromJS(formData.uiSchema),
      selectedField: null
    })

    this.state = {
      data: new InitData()
    }
  }

  componentWillUpdate (nextProps) {
    if (
      nextProps.formData.recordType !== this.props.formData.recordType &&
      nextProps.formData.recordType
    ) {
      const {
        formContext: { recordTypes }
      } = this.props
      const data = recordTypes.find(r => r.id === nextProps.formData.recordType)
      this.setImmState(s =>
        s
          .set('schema', fromJS(JSON.parse(data.schema)))
          .set('uiSchema', fromJS(JSON.parse(data.uiSchema)))
      )
    }
    if (
      nextProps.formData.formType !== this.props.formData.formType &&
      nextProps.formData.formType === 'custom'
    ) {
      const { schema, registry = getDefaultRegistry() } = this.props
      const { definitions } = registry
      const defaults = getDefaultFormState(schema, null, definitions)
      this.setImmState(s =>
        s
          .set('schema', fromJS(defaults.schema))
          .set('uiSchema', fromJS(defaults.uiSchema))
      )
    }
  }

  updateParent = () => {
    const { schema, uiSchema } = this.state.data.toJS()
    const { formData } = this.props
    let newData
    if (formData.formType === 'record') {
      newData = { ...formData }
      delete newData.schema
      delete newData.uiSchema
      this.props.onChange(newData)
    } else {
      newData = { ...formData }
      delete newData.recordType
      this.props.onChange(newData, schema, uiSchema)
    }
  }

  setImmState = fn => {
    this.setState(
      ({ data }) => ({
        data: fn(data)
      }),
      () => this.updateParent()
    )
  }

  setFormType = formType => {
    const { schema, uiSchema } = this.state.data.toJS()
    this.props.onChange({ schema, uiSchema, formType })
  }

  renderChoice = () => {
    return (
      <Row style={{ marginTop: 30 }}>
        <Col span={12}>
          <Empty
            description='Choose an existing record type...'
            image={
              <img
                width='100px'
                height='100px'
                src='https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-300x300.png'
                className='attachment-medium size-medium'
                alt='icon indicating a form'
                srcset='https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-300x300.png 300w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-150x150.png 150w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-768x768.png 768w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-105x105.png 105w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-317x317.png 317w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-423x423.png 423w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-634x634.png 634w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-846x846.png 846w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-951x951.png 951w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon-32x32.png 32w, https://umaine.edu/umss/wp-content/uploads/sites/458/2018/06/Forms-icon.png 968w'
              />
            }
          >
            <Button type='primary' onClick={() => this.setFormType('record')}>
              Continue as Record Type
            </Button>
          </Empty>
        </Col>
        <Col span={12}>
          <Empty
            description='Create a custom form...'
            image={
              <img
                class='irc_mi'
                src='https://p16cdn4static.sharpschool.com/UserFiles/Servers/Server_79234/Image/Departments/Facilities%20and%20Operations/Facility%20Rentals/Facility-Request-Form.png'
                alt='Image result for form icon'
                width='100px'
                height='100px'
                data-iml='1555268833210'
              />
            }
          >
            <Button onClick={() => this.setFormType('custom')}>
              Continue as Custom
            </Button>
          </Empty>
        </Col>
      </Row>
    )
  }

  render = () => {
    const { data } = this.state
    const {
      uiSchema,
      formData,
      errorSchema,
      idSchema,
      name,
      required,
      disabled,
      readonly,
      idPrefix,
      onBlur,
      onFocus,
      registry = getDefaultRegistry()
    } = this.props
    const { definitions, fields, formContext } = registry
    const { SchemaField, TitleField, DescriptionField } = fields
    const schema = retrieveSchema(this.props.schema, definitions, formData)
    const title = schema.title === undefined ? name : schema.title
    const description = uiSchema['ui:description'] || schema.description
    let orderedProperties
    try {
      const properties = Object.keys(schema.properties || {}).filter(
        p => !['schema', 'uiSchema'].includes(p)
      )
      orderedProperties = orderProperties(properties, uiSchema['ui:order'])
    } catch (err) {
      return (
        <div>
          <p className='config-error' style={{ color: 'red' }}>
            Invalid {name || 'root'} object field configuration:
            <em>{err.message}</em>.
          </p>
          <pre>{JSON.stringify(schema)}</pre>
        </div>
      )
    }
    const Template = registry.ObjectFieldTemplate || DefaultObjectFieldTemplate
    const templateProps = {
      title: uiSchema['ui:title'] || title,
      description,
      TitleField,
      DescriptionField,
      properties: orderedProperties.map(name => {
        const addedByAdditionalProperties = schema.properties[
          name
        ].hasOwnProperty(ADDITIONAL_PROPERTY_FLAG)
        return {
          content: (
            <SchemaField
              key={name}
              name={name}
              required={this.isRequired(name)}
              schema={schema.properties[name]}
              uiSchema={
                addedByAdditionalProperties
                  ? uiSchema.additionalProperties
                  : uiSchema[name]
              }
              errorSchema={errorSchema[name]}
              idSchema={idSchema[name]}
              idPrefix={idPrefix}
              formData={(formData || {})[name]}
              onKeyChange={this.onKeyChange(name)}
              onChange={this.onPropertyChange(
                name,
                addedByAdditionalProperties
              )}
              onBlur={onBlur}
              onFocus={onFocus}
              registry={registry}
              disabled={disabled}
              readonly={readonly}
              onDropPropertyClick={this.onDropPropertyClick}
            />
          ),
          name,
          readonly,
          disabled,
          required
        }
      }),
      readonly,
      disabled,
      required,
      idSchema,
      uiSchema,
      schema,
      formData,
      formContext
    }

    return (
      <div>
        {formData.formType ? (
          <React.Fragment>
            <Template {...templateProps} onAddClick={this.handleAddClick} />
            {formData.formType === 'custom' || formData.recordType ? (
              <FormBuilder
                fieldsOnly
                data={data}
                setImmState={this.setImmState}
                preview={formData.formType === 'record'}
              />
            ) : null}
          </React.Fragment>
        ) : (
          this.renderChoice()
        )}
      </div>
    )
  }
}

export default JsonFormField
