import React from 'react'
import ReactDOM from 'react-dom'
import { Dropdown, Button, Menu, Icon, Timeline } from 'antd'
import ScrollIntoViewIfNeeded from 'react-scroll-into-view-if-needed'
import classnames from 'classnames'
import styles from './StepsArray.module.scss'

function StepArrayItemTemplate ({
  index,
  children,
  hasMoveUp,
  hasMoveDown,
  onReorderClick,
  onDropIndexClick,
  selected,
  onAddClick
}) {
  const contextMenu = (
    <Menu className='drop-menu'>
      {hasMoveUp && (
        <Menu.Item onClick={onReorderClick(index, index - 1)}>
          <Icon type='arrow-up' />
          Move Up
        </Menu.Item>
      )}
      {hasMoveDown && (
        <Menu.Item onClick={onReorderClick(index, index + 1)}>
          <Icon type='arrow-down' />
          Move Down
        </Menu.Item>
      )}
      <Menu.Item onClick={onDropIndexClick(index)}>
        <Icon type='delete' />
        Delete
      </Menu.Item>
    </Menu>
  )

  return (
    <div key={index}>
      <Dropdown trigger={['click']} overlay={contextMenu} placement='topRight'>
        <Icon type='more' />
      </Dropdown>
      {children}
    </div>
  )
}

class StepsArrayTemplate extends React.Component {
  render () {
    const { canAdd, onAddClick, items, formContext, ...otherProps } = this.props
    const { selectedStep } = formContext.builder
    const menu = (
      <Menu>
        <Menu.Item key='0' onClick={() => onAddClick(null, 'UserTask')}>
          <Icon type='user' />
          User Task
        </Menu.Item>
        <Menu.Item key='1' onClick={() => onAddClick(null, 'ServiceTask')}>
          <Icon type='robot' />
          Web Service
        </Menu.Item>
      </Menu>
    )
    return (
      <div className={styles.default}>
        <Dropdown
          overlayStyle={{ zIndex: 2000 }}
          overlay={menu}
          trigger={['click']}
          placement='bottomCenter'
        >
          <Button type='primary'>
            <Icon type='plus-circle' /> New Step
          </Button>
        </Dropdown>
      </div>
    )
  }
}

export default StepsArrayTemplate
