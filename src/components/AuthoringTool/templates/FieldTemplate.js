import React from 'react'
import classnames from 'classnames'
import { Form } from 'antd'
import { getUiOptions } from 'react-jsonschema-form/lib/utils'

function DefaultTemplate (props) {
  const {
    id,
    label,
    children,
    errors,
    help,
    description,
    hidden,
    required,
    displayLabel,
    formContext,
    classNames,
    rawErrors,
    rawHelp,
    uiSchema,
    schema,
    ...otherProps
  } = props

  if (hidden) {
    return children
  }

  const { labelCol, wrapperCol, colon } = getUiOptions(uiSchema)
  let className = classNames.replace('form-group', '').trim()

  const formGroupProps = {
    id,
    colon,
    className: classnames(className),
    help: rawHelp,
    label: displayLabel ? label : false,
    labelCol: { span: labelCol || 6 },
    wrapperCol: { span: wrapperCol || 14 }
  }

  if (rawErrors && rawErrors.length) {
    formGroupProps.validateStatus = 'error'
    formGroupProps.help = rawErrors[0]
    formGroupProps.hasFeedback = schema.type !== 'boolean'
  }

  if (id === 'root' || id === 'root_steps') {
    return children
  }

  const stepNode = id.match(/^root_steps_\d+$/) || {}
  const actionsNode = id.match(/^root_steps_\d+_actions$/) || {}
  if (stepNode && id === stepNode.input) {
    return children
  }

  if (actionsNode && id === actionsNode.input) {
    return children
  }

  if (schema.type === 'object') {
    return children
  }

  return <Form.Item {...formGroupProps}>{children}</Form.Item>
}

export default DefaultTemplate
