import React from 'react'
import { Timeline, Icon, Avatar, Tooltip } from 'antd'
import styles from './WorkflowTemplate.module.scss'
import cx from 'classnames'

function WorkflowTemplate ({
  TitleField,
  properties,
  description,
  DescriptionField,
  idSchema,
  addField,
  formContext,
  schema,
  formData,
  uiSchema,
  ...otherProps
}) {
  const { $id } = idSchema
  const { builder } = formContext
  let steps
  const { classNames, colors, icons, labels } = uiSchema['ui:options']
  const { selectedStep } = builder

  switch ($id) {
    case 'root':
      steps = properties.find(p => p.name === 'steps')

      return (
        <div>
          {steps.content}
          <div className={styles['steps-content']}>
            <Timeline>
              {formData.steps.map((el, i) => {
                const dot = (
                  <Avatar
                    icon={icons[el.type]}
                    className={cx(styles[el.type], {
                      active: i === selectedStep
                    })}
                    onClick={() => builder.selectStep(i)}
                  />
                )
                return (
                  <Timeline.Item
                    onClick={() => builder.selectStep(i)}
                    key={i}
                    dot={
                      <Tooltip
                        visible
                        placement='right'
                        contentStyle={{ backgroundColor: '#4A4A4A' }}
                        title={
                          <div style={{ wordBreak: 'break-all' }}>
                            {el.name
                              ? `${labels[el.type]}: ${el.name}`
                              : labels[el.type]}
                          </div>
                        }
                      >
                        {dot}
                      </Tooltip>
                    }
                  >
                    <div className={styles.spacer} />
                  </Timeline.Item>
                )
              })}
            </Timeline>
          </div>
        </div>
      )
    default:
      return (
        <div className='column col-12'>
          <h5 style={{ marginBottom: '1em' }}>{TitleField}</h5>
          <div className='form-description'>
            {DescriptionField ? (
              <DescriptionField description={description} />
            ) : (
              { description }
            )}
          </div>
          {properties.map(({ content }) => content)}
        </div>
      )
  }
}

export default WorkflowTemplate
