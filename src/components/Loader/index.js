import React from 'react'
import { Dimmer, Loader, Message, Segment, Image } from 'semantic-ui-react'
import pImg from './paragraph.png'

const LoaderBase = () => (
  <Dimmer active page style={{ opacity: 0.6 }}>
    <Loader active inline='centered' size='huge'>
      Loading
    </Loader>
  </Dimmer>
)

export const ContainerLoader = () => (
  <Loader active inline='centered' size='huge'>
    Loading
  </Loader>
)

export const AppLoader = () => <LoaderBase />

export const ContentLoader = () => (
  <Segment loading>
    <Image src={pImg} />
  </Segment>
)

export const PageLoader = props => {
  if (props.loading) {
    if (props.timedOut) {
      return (
        <Message
          negative
          header='Request Timeout'
          content='There was a timeout while fetching data from the server.'
        />
      )
    } else if (props.pastDelay) {
      return <LoaderBase />
    } else {
      return null
    }
  } else if (props.error) {
    return (
      <Message
        negative
        header='Request Error'
        content='There was an error while fetching data from the server.'
      />
    )
  } else {
    return null
  }
}

export default { PageLoader, AppLoader }
