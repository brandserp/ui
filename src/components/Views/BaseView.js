import React, { Component } from 'react'
import { Grid, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import { ContainerLoader } from 'components/Loader'
import ResourceNotFound from './ResourceNotFound'

class BaseView extends Component {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    title: PropTypes.string,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  componentDidMount () {
    const { navigationViewController, navView, prefetch } = this.props
    if (navView) {
    }
    if (prefetch.length) {
      prefetch.forEach(p => p())
    }
  }

  componentDidUpdate (prevProps) {
    const { prefetch, match } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }
  }

  renderContent = () => {
    const { title, children } = this.props
    return children
  }

  render () {
    const { loading, resourceName, item } = this.props
    if (loading) {
      return <ContainerLoader />
    } else {
      return item ? this.renderContent() : <ResourceNotFound />
    }
  }
}

export default BaseView
