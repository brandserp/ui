import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon } from 'semantic-ui-react'
import { Route, Switch } from 'react-router-dom'
import PageHeader from 'ant-design-pro/lib/PageHeader'

import {
  Drawer,
  Dropdown,
  Table,
  Spin,
  Button,
  Menu,
  Tag,
  Icon as AntIcon,
  Radio,
  Popconfirm,
  Col,
  Row,
  Input,
  Select
} from 'antd'

import { Resizable } from 'react-resizable'

import { ContainerLoader } from 'components/Loader'
import { types as operatorTypes, taskTypes, ops } from 'utils/filters'
import store from 'store'
import ViewOptions from './ViewOptions'

const Option = Select.Option
const SubMenu = Menu.SubMenu

Spin.setDefaultIndicator(<AntIcon type='loading' spin />)

const ResizeableTable = props => {
  const { onResize, width, ...restProps } = props

  if (!width) {
    return <th {...restProps} />
  }

  return (
    <Resizable width={width} height={0} onResize={onResize}>
      <th {...restProps} />
    </Resizable>
  )
}

const defaultFilter = {
  column: 'id',
  operator: 'eq',
  value: ''
}
const taskDefaultFilter = {
  column: 'name',
  operator: 'eq',
  value: ''
}
const viewTitles = {
  saveAs: 'Save New View',
  rename: 'Rename Current View'
}

class MasterListView extends Component {
  static propTypes = {
    items: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    itemRenderer: PropTypes.func,
    detailView: PropTypes.func,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    shortcutActions: PropTypes.array,
    slideoutWidth: PropTypes.number,
    customViews: PropTypes.bool,
    view: PropTypes.object,
    saveView: PropTypes.func,
    viewType: PropTypes.string
  }

  static defautProps = {
    customViews: false
  }

  static contextTypes = {
    router: PropTypes.object
  }

  components = {
    header: {
      cell: ResizeableTable
    }
  }

  constructor (props) {
    super(props)
    const { resourceList, resourceName, searchJoin } = props
    this.isTask = resourceName.route === '/tasks'
    const search = this.isTask ? taskDefaultFilter : defaultFilter

    this.state = {
      columns: resourceList(store.getState()).columns,
      selected: [],
      search,
      searchJoin: searchJoin || undefined,
      viewOptions: false,
      viewOptionsMode: null,
      filterPopup: false,
      removeMode: false,
      removeTarget: null
    }
  }

  updateForm = (e, name) => {
    const data = {}
    name === 'value' ? (data[name] = e.target.value) : (data[name] = e)
    const slice = {}
    slice['search'] = {
      ...this.state['search'],
      ...data
    }
    this.setState(slice)
  }
  goTo = location => {
    this.context.router.history.push(location)
  }

  componentWillMount () {
    const { defaultView, changeParams, authUser } = this.props
    if (defaultView === 'self') {
      changeParams({
        search: [
          {
            column: this.isTask ? 'assignee' : 'userId',
            operator: 'eq',
            value: authUser.id
          }
        ]
      })
    }
  }

  componentDidMount () {
    const {
      refetch,
      prefetch,
      match,
      location,
      resourceName,
      viewId
    } = this.props
    if (prefetch.length && match.path === `${resourceName.route}`) {
      prefetch.forEach(p => p())
      if (!viewId) {
        refetch()
      }
    }
  }

  componentDidUpdate (prevProps) {
    const {
      prefetch,
      match,
      params,
      refetch,
      view,
      changeParams,
      resetParams,
      viewId,
      authUser,
      defaultView
    } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }

    if (prevProps.defaultView !== defaultView || prevProps.viewId !== viewId) {
      resetParams()

      if (defaultView === 'self') {
        changeParams({
          search: [
            {
              column: this.isTask ? 'assignee' : 'userId',
              operator: 'eq',
              value: authUser.id
            }
          ]
        })
      }
    }

    if (prevProps.view !== view) {
      if (view && viewId) {
        const viewData = view.toJS()
        changeParams(viewData.query)
      } else {
        resetParams()
      }
    }

    if (prevProps.params !== params) {
      refetch()
    }
  }

  componentWillUnmount () {
    this.props.resetParams()
  }

  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns]
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width
      }
      return { columns: nextColumns }
    })
  }

  triggerRemove = index => {
    this.setState({
      removeMode: true,
      removeTarget: index
    })
  }

  clearRemove = () => {
    this.setState({
      removeMode: false,
      removeTarget: null
    })
  }

  toggleFilter = () => {
    this.setState({
      filterPopup: !this.state.filterPopup,
      search: this.isTask ? taskDefaultFilter : defaultFilter
    })
  }

  toggleOptions = mode => {
    this.setState({
      viewOptions: !this.state.viewOptions,
      viewOptionsMode: mode
    })
  }

  removeResource = id => {
    this.props.removeAction(id)
  }

  renderConfirm = () => {
    const { removeTarget } = this.state
    const { resourceName } = this.props
    return (
      <Modal
        size='mini'
        open
        basic
        closeIcon
        onClose={() => this.clearRemove()}
      >
        <Modal.Header>
          Delete {`${resourceName.singular} - ${removeTarget}`}
        </Modal.Header>
        <Modal.Content>
          <p>
            Are you sure you want to delete this {`${resourceName.singular}`}?
          </p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            negative
            icon='checkmark'
            labelPosition='right'
            content='Delete'
            onClick={() => this.removeResource(removeTarget)}
          />
        </Modal.Actions>
      </Modal>
    )
  }

  toggleSelection = id => {
    const { selected } = this.state
    if (selected.includes(id)) {
      selected.splice(selected.indexOf(id), 1)
    } else {
      selected.push(id)
    }
    this.setState({
      selected
    })
  }

  handleTableChange = (pagination, filters, sorter) => {
    const { changeParams } = this.props
    const sort = {
      sortField: sorter.field,
      sortOrder: sorter.order,
      ...filters
    }
    changeParams(sort)
  }

  handleSearchChange = e => {
    e.preventDefault()
    const { search } = this.state
    this.toggleFilter()
    this.props.changeParams({ search: [search] })
  }

  removeFilter = id => {
    const { params } = this.props
    this.props.changeParams({
      removeFilter: params.get('search').findIndex(f => f.get('id') === id)
    })
  }

  onCreateView = data => {
    const { saveView, viewType, match } = this.props
    const newView = {
      ...data,
      type: viewType
    }
    saveView(newView).then(({ id }) => {
      this.goTo(`${match.path}?view=${id}`)
      this.toggleOptions()
    })
  }

  onUpdateView = () => {
    const { viewId } = this.props
    this.props.updateView(viewId)
  }

  onRenameView = data => {
    const { renameView } = this.props
    renameView(data).then(() => {
      this.toggleOptions()
    })
  }

  onDeleteView = () => {
    const { viewId, deleteView, match } = this.props
    deleteView(viewId).then(() => {
      this.goTo(`${match.path}`)
    })
  }

  getCreateButton = () => {
    const { createOptions, resourceName } = this.props
    if (createOptions) {
      const items = createOptions.map((v, k) => (
        <Menu.Item
          onClick={() => this.goTo(`${resourceName.route}/new?mode=${v.key}`)}
        >
          {v.label}
        </Menu.Item>
      ))
      return (
        <Dropdown
          overlay={<Menu>{items}</Menu>}
          trigger={['click']}
          placement='bottomRight'
        >
          <Button type='primary' icon='plus-circle'>
            {`New ${resourceName.singular}`}
          </Button>
        </Dropdown>
      )
    } else {
      return (
        <Button
          type='primary'
          icon='plus-circle'
          onClick={() => this.goTo(`${resourceName.route}/new`)}
        >
          {`New ${resourceName.singular}`}
        </Button>
      )
    }
  }

  render () {
    const {
      loading,
      resourceName,
      detailView,
      createView,
      match,
      items,
      params,
      slideoutWidth,
      customViews,
      view,
      defaultView,
      completeView,
      shortcutActions,
      removeAction
    } = this.props
    const {
      removeMode,
      columns,
      search,
      viewOptions,
      viewOptionsMode,
      filterPopup
    } = this.state
    const Detail = detailView
    const Create = createView
    const Complete = completeView
    let searchJoin = params ? params.get('searchJoin') : null

    const createBtn = createView ? this.getCreateButton() : null
    const viewActions = (
      <Menu>
        {view ? (
          <Menu.Item onClick={() => this.onUpdateView()}>
            <Icon name='save' text='Save' />
            Save
          </Menu.Item>
        ) : null}
        <Menu.Item onClick={() => this.toggleOptions('saveAs')}>
          <Icon name='save outline' />
          Save as...
        </Menu.Item>
        {view ? (
          <Menu.Item onClick={() => this.toggleOptions('rename')}>
            <Icon name='pen square' />
            Rename
          </Menu.Item>
        ) : null}
        {view ? (
          <Menu.Item>
            <Popconfirm
              title='Are you sure you want to delete this view?'
              placement='left'
              onConfirm={() => this.onDeleteView()}
            >
              <Icon name='trash' />
              Delete
            </Popconfirm>
          </Menu.Item>
        ) : null}
      </Menu>
    )

    const saveCustomView = customViews ? (
      <Dropdown
        overlay={viewActions}
        trigger={['click']}
        placement='bottomRight'
      >
        <Button>
          View Options
          <AntIcon type='down' />
        </Button>
      </Dropdown>
    ) : null

    const contextMenu = item => (
      <Menu placement='bottom'>
        {shortcutActions
          ? shortcutActions(item).map((v, k) => (
            <Menu.Item onClick={v.onClick} key={k}>
              <Icon name={v.icon} />
              {v.label}
            </Menu.Item>
          ))
          : null}
        {shortcutActions && removeAction ? <Menu.Divider /> : null}
        {removeAction ? (
          <Menu.Item key='delete'>
            <Popconfirm
              title='Are you sure you want to delete this item?'
              placement='left'
              onConfirm={() => this.removeResource(item.id)}
            >
              <Icon name='trash' />
              Delete
            </Popconfirm>
          </Menu.Item>
        ) : null}
      </Menu>
    )

    const resourceCols = columns.map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: this.handleResize(index)
      })
    }))

    const tableProps = {
      className: 'data-table',
      dataSource: items.toJS(),
      columns: [
        ...resourceCols,
        {
          title: 'Actions',
          key: 'actions',
          width: 50,
          align: 'center',
          render: (text, record) => {
            return (
              <div onClick={e => e.stopPropagation()}>
                <Dropdown
                  overlay={() => contextMenu(record)}
                  trigger={['click']}
                >
                  <Button type='dashed' shape='circle' icon='ellipsis' />
                </Dropdown>
              </div>
            )
          }
        }
      ],
      components: this.components,
      rowKey: record => record.id,
      onChange: this.handleTableChange,
      loading,
      size: 'middle',
      bordered: true,
      onRow: record => {
        return {
          onClick: e => {
            const isMyTasks =
              this.isTask && defaultView === 'self' ? '/complete' : ''
            this.goTo(`${resourceName.route}/${record.id}${isMyTasks}`)
          }
        }
      }
    }

    const opTypes = this.isTask ? taskTypes : operatorTypes

    const filters = params.get('search').toJS()

    const filterOps = columns
      .filter(c => c.filter)
      .map((c, i) => ({
        key: c.dataIndex,
        text: c.title,
        value: c.dataIndex
      }))
    const filtCol = columns.find(c => c.dataIndex === search.column)
    const opKey = this.isTask ? filtCol.dataIndex : filtCol.type
    const operatorOps = opTypes[opKey].map((c, i) => ({
      key: c.id,
      text: c.title,
      value: c.id
    }))

    const selectBoxStyle = {
      width: 745
    }

    const filterTagList = (
      <div>
        <Menu
          style={{
            backgroundColor: 'transparent',
            border: 'none',
            border: '1px solid #D9D9D9',
            borderRadius: 4
          }}
          mode='inline'
          selectable={false}
        >
          <SubMenu
            key='sub2'
            title={
              <span>
                <AntIcon type='search' />
                <span style={{ marginRight: 12, color: 'primary' }}>
                  Search
                </span>
                <span>
                  {filters.map((v, k) => {
                    return (
                      <Tag
                        color='blue'
                        key={k}
                        closable
                        onClose={e => e.stopPropagation()}
                        afterClose={() => this.removeFilter(v.operator)}
                      >
                        {`${v.column} ${ops[v.operator].symbol} '${v.value}'`}
                      </Tag>
                    )
                  })}
                </span>
              </span>
            }
          >
            <Menu.Divider />
            <Menu.Item style={{ marginTop: 20, cursor: 'default' }}>
              <Row>
                <Col span={3}>Column</Col>
                <Col span={12}>
                  <Select
                    showSearch
                    style={selectBoxStyle}
                    optionFilterProp='children'
                    onChange={e => this.updateForm(e, 'column')}
                  >
                    {filterOps.map(c => (
                      <Option key={c.key} value={c.value}>
                        {c.text}
                      </Option>
                    ))}
                  </Select>
                </Col>
              </Row>
            </Menu.Item>
            <Menu.Item style={{ cursor: 'default' }}>
              <Row>
                <Col span={3}>Operator</Col>
                <Col span={12}>
                  <Select
                    showSearch
                    style={selectBoxStyle}
                    optionFilterProp='children'
                    onChange={e => this.updateForm(e, 'operator')}
                  >
                    {operatorOps.map(c => (
                      <Option key={c.key} value={c.value}>
                        {c.text}
                      </Option>
                    ))}
                  </Select>
                </Col>
              </Row>
            </Menu.Item>
            <Menu.Item style={{ cursor: 'default' }}>
              <Row>
                <Col span={3}>Value</Col>
                <Col span={12}>
                  <Input
                    value={search.value}
                    style={selectBoxStyle}
                    onChange={e => this.updateForm(e, 'value')}
                  />
                </Col>
              </Row>
            </Menu.Item>
            <Menu.Item style={{ cursor: 'default' }}>{joinOp}</Menu.Item>
            <Menu.Item style={{ cursor: 'default' }}>
              <Button
                type='primary'
                style={{ float: 'right' }}
                onClick={this.handleSearchChange}
                disabled={!search.value}
              >
                Save
              </Button>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
    )

    const joinOp =
      searchJoin && filters.length > 1 ? (
        <Radio.Group defaultValue={searchJoin} style={{ marginRight: 8 }}>
          <Radio.Button value='and'>AND</Radio.Button>
          <Radio.Button value='or'>OR</Radio.Button>
        </Radio.Group>
      ) : null

    // ask about this saveCustomView
    // and some other missed functionallity if we still need this
    const actions = (
      <div>
        <Button.Group>{customViews ? saveCustomView : null}</Button.Group>
        {createBtn}
      </div>
    )

    return (
      <div className='master-list'>
        {removeMode ? this.renderConfirm() : null}
        <Drawer
          placement='left'
          visible={viewOptions}
          width={360}
          title={`View Options - ${viewTitles[viewOptionsMode]}`}
          onClose={() => this.toggleOptions(null)}
        >
          <ViewOptions
            mode={viewOptionsMode}
            view={view}
            onCreate={this.onCreateView}
            onRename={this.onRenameView}
          />
        </Drawer>
        <Drawer
          placement='right'
          visible={
            this.context.router.route.location.pathname !== resourceName.route
          }
          width={slideoutWidth || 720}
          onClose={() => this.goTo(`${resourceName.route}`)}
        >
          {loading ? (
            <ContainerLoader />
          ) : (
            <Switch>
              <Route
                exact
                path={`${match.url}/new`}
                render={props => <Create {...props} />}
              />
              <Route
                exact
                path={`${match.url}/:id`}
                render={props => {
                  return <Detail {...props} />
                }}
              />
              <Route
                exact
                path={`/tasks/:id/complete`}
                render={props => {
                  return <Complete {...props} />
                }}
              />
            </Switch>
          )}
        </Drawer>
        <div className='list-wrapper'>
          <PageHeader
            title={resourceName.plural}
            hiddenBreadcrumb
            action={actions}
            content={filterTagList}
          />
          <Table {...tableProps} />
        </div>
      </div>
    )
  }
}

export default MasterListView
