import React from 'react'
import { Form, Input, Button } from 'antd'

class ViewOptions extends React.Component {
  handleSubmit = e => {
    const { form, onCreate, onRename, view, mode } = this.props
    e.preventDefault()
    form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
      }
      if (mode === 'rename') {
        onRename({
          id: view.id,
          ...values
        })
      } else {
        onCreate(values)
      }
    })
  }

  render () {
    const { form, view } = this.props
    return (
      <Form layout='vertical' onSubmit={this.handleSubmit}>
        <Form.Item label='Name'>
          {form.getFieldDecorator('name', {
            initialValue: view ? view.name : '',
            rules: [
              { required: true, message: 'Please input the name of view.' }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item>
          <Button type='primary' htmlType='submit'>
            Submit
          </Button>
        </Form.Item>
      </Form>
    )
  }
}

const WrappedViewOptions = Form.create()(ViewOptions)

export default WrappedViewOptions
