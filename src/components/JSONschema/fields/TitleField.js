import React from 'react'
import PropTypes from 'prop-types'

const REQUIRED_FIELD_SYMBOL = '*'

function TitleField (props) {
  const { id, title, required } = props
  if (!title) {
    return null
  }
  return (
    <legend id={id}>
      {title}
      {required && <span className='required'>{REQUIRED_FIELD_SYMBOL}</span>}
    </legend>
  )
}

export default TitleField
