import React from 'react'
import mitt from 'mitt'

import WorkflowEditorParent from './WorkflowEditorParent'
import TabsProvider from './TabsProvider'

const eventBus = new mitt()
const tabsProvider = new TabsProvider()

const globals = {
  eventBus
}

export default props => {
  const mergeProps = {
    tabsProvider,
    globals,
    ...props
  }
  return <WorkflowEditorParent id='bpmn-diagram' {...mergeProps} />
}
