import React, { Component } from 'react'

import WorkflowEditor from './WorkflowEditor'

export default class WorkflowEditorParent extends Component {
  constructor () {
    super()

    this.appRef = React.createRef()
  }

  triggerAction = (event, action, options) => {
    console.log('trigger action', action, options)

    const result = this.getWorkflowEditor().triggerAction(action, options)

    if (result && 'catch' in result) {
      result.catch(() => console.error)
    }
  }

  handleError = (error, tab) => {
    if (tab) {
      return console.log('tab error', error, tab)
    }

    return console.log('app error', error)
  }

  handleReady = async () => {
    const { tabsProvider, workflow } = this.props

    const initialTab = workflow
      ? tabsProvider.createTabForFile({
        name: `${workflow.bpmnId}.bpmn`,
        path: null,
        contents: workflow.bpmnXml
      })
      : tabsProvider.createTab('bpmn')
    return initialTab
  }

  getWorkflowEditor () {
    return this.appRef.current
  }

  render () {
    const {
      tabsProvider,
      globals,
      canSave,
      canDuplicate,
      canPublish,
      onSave,
      onPublish,
      onUnpublish,
      onDuplicate,
      published,
      openDrawer,
      workflowBuilderData
    } = this.props

    const enablePublish = canPublish && !published ? onPublish : false
    const enableUnpublish = published && !canPublish ? onUnpublish : false
    const enableDuplicate = canDuplicate ? onDuplicate : false
    return (
      <WorkflowEditor
        ref={this.appRef}
        tabsProvider={tabsProvider}
        globals={globals}
        onReady={this.handleReady}
        onError={this.handleError}
        onPublish={enablePublish}
        onUnpublish={enableUnpublish}
        onDuplicate={enableDuplicate}
        openDrawer={openDrawer}
        workflowBuilderData={workflowBuilderData}
        onSave={onSave}
      />
    )
  }
}
