import { Record } from 'immutable'
import { schema } from 'normalizr'
import _ from 'lodash'

import { extendFactory } from 'models/base'
import { roleSchema } from './roles'

export const userSchema = new schema.Entity('users', {
  roles: [roleSchema]
})

const User = new Record({
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  roles: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'User',
    plural: 'Users',
    route: '/users'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'firstName',
        title: 'First Name',
        sorter: true,
        width: 50,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'lastName',
        title: 'Last Name',
        sorter: true,
        width: 50,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'email',
        title: 'E-mail',
        sorter: true,
        width: 100,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: deps => ({
    schema: {
      type: 'object',
      properties: {
        firstName: {
          type: 'string',
          title: 'First Name',
          minLength: 1
        },
        lastName: {
          type: 'string',
          title: 'Last Name',
          minLength: 1
        },
        email: {
          type: 'string',
          title: 'E-mail',
          format: 'email',
          minLength: 6
        },
        roles: {
          type: 'array',
          uniqueItems: true,
          title: 'Roles',
          items: {
            type: 'number',
            anyOf: deps.roles.map(r => ({
              type: 'number',
              enum: [r.id],
              title: r.name
            }))
          },
          multiple: true
        },
        password: {
          type: 'string',
          title: 'Password',
          minLength: 4
        },
        confirmPassword: {
          type: 'string',
          title: 'Confirm Password',
          minLength: 4
        }
      },
      required: [
        'firstName',
        'lastName',
        'email',
        'password',
        'confirmPassword'
      ],
      validations: [
        {
          type: 'equalsProp',
          key: 'confirmPassword',
          matchProp: 'password',
          message: 'Passwords must match.'
        },
        {
          type: 'equalsProp',
          key: 'password',
          matchProp: 'confirmPassword',
          message: 'Passwords must match.'
        }
      ]
    },
    uiSchema: {
      firstName: {
        'ui:emptyValue': '',
        'ui:placeholder': 'First Name',
        'ui:options': {
          wrapperCol: 12
        }
      },
      lastName: {
        'ui:emptyValue': '',
        'ui:placeholder': 'Last Name',
        'ui:options': {
          wrapperCol: 12
        }
      },
      roles: {
        'ui:emptyValue': [],
        'ui:options': {
          mode: 'multiple',
          wrapperCol: 12,
          orderable: false,
          placeholder: 'Choose role(s)'
        }
      },
      email: {
        'ui:emptyValue': '',
        'ui:placeholder': 'E-mail',
        'ui:options': {
          wrapperCol: 12
        }
      },
      password: {
        'ui:emptyValue': '',
        'ui:placeholder': 'Password',
        'ui:options': {
          wrapperCol: 12
        }
      },
      confirmPassword: {
        'ui:placeholder': 'Confirm password',
        'ui:options': {
          wrapperCol: 12
        }
      }
    }
  }),
  schemaName: 'users',
  recordType: User,
  modelSchema: userSchema
}

const model = extendFactory(modelConfig)

export default model
