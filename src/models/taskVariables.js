import { Record, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { extendFactory } from 'models/base'
import api from 'middleware/camunda'

const taskVariablesSchema = new schema.Entity(
  'taskVariables',
  {},
  {
    processStrategy: ({ formVariables, id }) => {
      const data = { formVariables, id }
      if (data.formVariables.formType === 'custom') {
        data.formVariables.schema = JSON.parse(`${formVariables.schema.value}`)
        data.formVariables.uiSchema = JSON.parse(
          `${formVariables.uiSchema.value}`
        )
      }

      return data
    }
  }
)

export const TaskVariables = new Record({
  id: null,
  formVariables: fromJS({})
})

const modelConfig = {
  resourceName: {
    singular: 'TaskVariable',
    plural: 'TaskVariables'
  },
  schemaName: 'taskVariables',
  recordType: TaskVariables,
  modelSchema: taskVariablesSchema
}

const modelEffects = config => ({
  async fetch ({ id, params }) {
    const { modelSchema } = config
    const res = await api.get(`/task/${id}/form-variables`, { params })
    this.merge(normalize({ formVariables: res.data, id }, modelSchema))
    return res.data
  }
})

const modelReducers = config => ({})

const modelSelectors = (slice, createSelector, hasProps) => ({
  getOne () {
    return createSelector(
      this.getList,
      (state, props) => props.id,
      (items, id) => items.get(id)
    )
  }
})

const model = extendFactory(
  modelConfig,
  modelReducers,
  modelEffects,
  modelSelectors
)

export default model
