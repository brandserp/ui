import { Record } from 'immutable'
import { schema } from 'normalizr'

import { extendFactory } from 'models/base'

export const taskSchema = new schema.Entity('tasks')

const TaskType = new Record({
  id: null,
  name: null,
  config: null,
  createdAt: null,
  updatedAt: null
})

const modelConfig = {
  resourceName: {
    singular: 'Task Type',
    plural: 'Task Types'
  },
  schemaName: 'taskTypes',
  apiPath: 'task_types',
  recordType: TaskType,
  modelSchema: taskSchema
}

const model = extendFactory(modelConfig)

export default model
