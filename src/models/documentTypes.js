import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'

import { extendFactory } from 'models/base'
import api from 'middleware/api'
import store from 'store'

export const DocumentTypeSchema = new schema.Entity('documentTypes')

export const DocumentTypeListSchema = new schema.Array(DocumentTypeSchema)

const DocumentType = new Record({
  id: null,
  name: null,
  description: null,
  properties: {
    fileTypes: []
  },
  media: [],
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Document Type',
    plural: 'Document Types',
    route: '/document-types'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'description',
        title: 'Description',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: {},
  schemaName: 'documentTypes',
  modelSchema: DocumentTypeSchema,
  listSchema: DocumentTypeListSchema,
  recordType: DocumentType,
  apiPath: 'document_types'
}

const modelEffects = config => ({
  async fetchWithMedia (id = null) {
    const { listSchema } = config
    const url = id === null ? '/documents' : `/documents/${id}`
    const res = await api.get(url, { with: 'media' })
    const normalized = normalize(res.data, listSchema)
    this.merge(normalized)
    store.dispatch.documents.merge(normalized)
    return res.data
  }
})

const modelReducers = config => ({})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
