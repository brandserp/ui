import { actions, reducer } from 'react-redux-toastr'

export default {
  baseReducer: reducer,
  effects: actions
}
