import { Record } from 'immutable'

import { extendFactory } from 'models/base'

const Comment = new Record({
  id: null,
  title: null,
  body: null,
  commentableId: null,
  commentableType: 'App\\Entities\\Collection',
  creatorId: null,
  creator: null,
  comments: [],
  createdAt: null,
  updatedAt: null
})

const modelConfig = {
  resourceName: {
    singular: 'Comment',
    plural: 'Comments'
  },
  schemaName: 'comments',
  recordType: Comment
}

const model = extendFactory(modelConfig)

export default model
