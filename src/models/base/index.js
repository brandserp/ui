import { Map, OrderedSet, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import _ from 'lodash'
import { toastr } from 'react-redux-toastr'

import api from 'middleware/api'
import { ucFirst } from 'utils/render'
import { buildParams } from 'utils/filters'

export const extendFactory = (
  config = {},
  reducers = c => ({}),
  effects = c => ({}),
  selectors = (slice, createSelector, hasProps) => ({})
) => {
  const baseConfig = {
    resourceName: {
      singular: 'Base',
      plural: 'Bases'
    },
    resourceListFields: state => [
      { property: 'id', label: 'ID' },
      { property: 'createdAt', label: 'Created At' },
      { property: 'updatedAt', label: 'Updated At' }
    ],
    schemaName: 'basemodel',
    recordType: null,
    modelSchema: null,
    listSchema: null,
    apiPath: null
  }

  let _config = { ...baseConfig, ...config }
  if (!_config.modelSchema) {
    _config.modelSchema = new schema.Entity(`${_config.schemaName}`)
  }
  if (!_config.listSchema) {
    _config.listSchema = new schema.Array(_config.modelSchema)
  }
  if (!_config.apiPath) {
    _config.apiPath = `${_config.schemaName}`
  }

  const initialParams = {
    sortField: null,
    sortOrder: null,
    searchJoin: null,
    search: []
  }

  const baseReducers = {
    delete: (state, id) => {
      let _state = state.deleteIn(['list', id])
      _state = _state.set('ids', _state.get('ids').subtract([id]))
      return _state
    },
    merge: (state, { entities, result, reset = false }) => {
      const { recordType, schemaName } = _config
      if (result) {
        const list = _.reduce(
          _.mapValues(entities[schemaName]),
          (res, value) => {
            return res.update(value.id, new recordType(), current => {
              return current.merge(value)
            })
          },
          state.get('list')
        )
        let ids
        if (reset) {
          ids = Array.isArray(result)
            ? OrderedSet(result)
            : OrderedSet([result])
        } else {
          ids = Array.isArray(result)
            ? state.get('ids').union(result)
            : state.get('ids').union([result])
        }
        return state.set('list', list).set('ids', ids)
      }
      return state
    },
    changeParams: (
      state,
      { sortField, sortOrder, search, searchJoin, removeFilter }
    ) => {
      let _state = state
      _state = sortField
        ? _state.setIn(['params', 'sortField'], sortField)
        : _state
      _state = sortOrder
        ? _state.setIn(['params', 'sortOrder'], sortOrder.replace('end', ''))
        : _state
      _state = search
        ? _state.setIn(
          ['params', 'search'],
          _state.getIn(['params', 'search']).merge(fromJS(search))
        )
        : _state
      _state = removeFilter
        ? _state.deleteIn(['params', 'search', removeFilter])
        : _state

      if (_state.getIn(['params', 'search']).count() > 1) {
        const op = searchJoin || 'and'
        _state = _state.setIn(['params', 'searchJoin'], op)
      } else {
        _state = searchJoin ? _state.deleteIn(['params', 'searchJoin']) : _state
      }
      return _state
    },
    resetParams: state => state.set('params', fromJS(initialParams)),
    search: (state, search) => state.set('search', fromJS(search))
  }

  const baseEffects = {
    remove (id) {
      const { apiPath, resourceName } = _config
      return api
        .delete(`${apiPath}/${id}`)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Deleted Successfully`)
          this.delete(parseInt(id))
          return data
        })
        .catch(e => console.error(e))
    },
    update (item) {
      const { apiPath, resourceName, modelSchema } = _config
      return api
        .post(`${apiPath}/${item.id}`, item)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Updated Successfully`)
          this.merge(normalize(data, modelSchema))
          return data
        })
        .catch(e => console.error(e))
    },
    create (item) {
      const { apiPath, resourceName, modelSchema } = _config
      return api
        .post(apiPath, item)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Created Successfully`)
          this.merge(normalize(data, modelSchema))
          return data
        })
        .catch(e => console.error(e))
    },
    async fetchAll (params = null, rootState) {
      const { listSchema, apiPath, schemaName } = _config
      const _params = params || buildParams(rootState[schemaName])
      const res = await api.get(`${apiPath}?${_params}`)
      const reset = _params !== ''
      this.merge({ reset, ...normalize(res.data, listSchema) })
      return res.data
    },
    async fetch ({ id, params }) {
      const { modelSchema, apiPath } = _config
      const res = await api.get(`${apiPath}/${id}`, { params })
      this.merge(normalize(res.data, modelSchema))
      return res.data
    }
  }

  return {
    state: fromJS({
      list: Map(),
      ids: OrderedSet(),
      params: initialParams
    }),
    reducers: { ...baseReducers, ...reducers(_config) },
    effects: { ...baseEffects, ...effects(_config) },
    selectors: (slice, createSelector, hasProps) => ({
      getParams: () =>
        createSelector(
          slice,
          s => s.get('params')
        ),
      getIds: () =>
        createSelector(
          slice,
          s => s.get('ids')
        ),
      getList: () =>
        createSelector(
          slice,
          s => s.get('list')
        ),
      getItems () {
        return createSelector(
          this.getIds,
          this.getList,
          (ids, list) => ids.map(i => list.get(i))
        )
      },
      getOne () {
        return createSelector(
          this.getList,
          (state, props) => props.id,
          (items, id) => items.get(parseInt(id))
        )
      },
      ...selectors(slice, createSelector, hasProps)
    })
  }
}
