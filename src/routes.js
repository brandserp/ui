import React from 'react'
import { ListCollections } from 'containers/Collections'
import { ListDocuments, ListPendingDocuments } from 'containers/Documents'
import { ListDocTypes } from 'containers/DocumentTypes'
import { ListUsers } from 'containers/Users'
import { ListWorkflows } from 'containers/Workflows'
import { ListRecordTypes } from 'containers/RecordTypes'
import { ListRecords } from 'containers/Records'
import { ListRoles } from 'containers/Roles'
import { ListPermissions } from 'containers/Permissions'
import { ListTasks, MyTasks } from 'containers/Tasks'
import { AccountingHome } from 'containers/Accounting'
import { SearchListMenu, SettingsMenu, AccountingMenu } from 'components/Navigation'

const views = {
  tasks: [
    { key: '/tasks', title: 'All Tasks' },
    { key: '/tasks?default=self', title: 'My Tasks' }
  ],
  documents: [{ key: '/documents', title: 'All Documents' }],
  collections: [{ key: '/collections', title: 'All Collections' }],
  records: [{ key: '/records', title: 'All Records' }]
}

export const resourceRoutes = [
  {
    path: '/collections',
    sider: props => (
      <SearchListMenu
        {...props}
        name='Collections'
        customViews={[]}
        defaultViews={views.collections}
      />
    ),
    content: ListCollections
  },
  {
    path: '/tasks',
    sider: props => (
      <SearchListMenu
        {...props}
        name='Tasks'
        customViews={[]}
        defaultViews={views.tasks}
      />
    ),
    content: ListTasks
  },
  {
    path: '/documents',
    sider: props => (
      <SearchListMenu
        {...props}
        name='Documents'
        customViews={[]}
        defaultViews={views.documents}
      />
    ),
    content: ListDocuments
  },
  {
    path: '/records',
    sider: props => (
      <SearchListMenu
        {...props}
        name='Records'
        customViews={[]}
        defaultViews={views.records}
      />
    ),
    content: ListRecords
  }
]

export const adminRoutes = [
  {
    path: '/document-types',
    sider: props => <SettingsMenu {...props} />,
    content: ListDocTypes
  },
  {
    path: '/pending-files',
    sider: props => <SettingsMenu {...props} />,
    content: ListPendingDocuments
  },
  {
    path: '/users',
    sider: props => <SettingsMenu {...props} />,
    content: ListUsers
  },
  {
    path: '/roles',
    sider: props => <SettingsMenu {...props} />,
    content: ListRoles
  },
  {
    path: '/permissions',
    sider: props => <SettingsMenu {...props} />,
    content: ListPermissions
  },
  {
    path: '/workflows',
    sider: props => <SettingsMenu {...props} />,
    content: ListWorkflows
  },
  {
    path: '/record-types',
    sider: props => <SettingsMenu {...props} />,
    content: ListRecordTypes
  },
  {
    path: '/accounting',
    sider: props => <AccountingMenu {...props} />,
    content: AccountingHome
  }
]
