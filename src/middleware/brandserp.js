import axios from 'axios'

const ERPAPI = axios.create({
  baseURL: process.env.REACT_APP_OLD_ERP_ENDPOINT
})

ERPAPI.interceptors.response.use(
  (response) => {
    console.log(response)
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)

ERPAPI.defaults.headers.post['Content-Type'] = 'text/html'

export default ERPAPI
