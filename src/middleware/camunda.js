import axios from 'axios'

const camAPI = axios.create({
  baseURL: process.env.REACT_APP_CAMUNDA_ENDPOINT,
  responseType: 'json'
})

camAPI.defaults.headers.post['Content-Type'] = 'application/json'
camAPI.defaults.headers.put['Content-Type'] = 'application/json'

export default camAPI
