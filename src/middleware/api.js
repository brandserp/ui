import axios from 'axios'
import applyConverters from 'axios-case-converter'

const token = localStorage.getItem('token')
const client = applyConverters(
  axios.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    responseType: 'json'
  })
)

client.defaults.headers.post['Content-Type'] = 'application/json'
if (token) {
  client.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export default client
