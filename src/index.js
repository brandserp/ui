import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { Switch, Route } from 'react-router'

import store from './store'
import { PublicOnlyRedir, AuthOnlyRedir } from 'utils/auth'
import LoginLayout from 'layouts/LoginLayout'
import LoginContainer from 'containers/Login'
import AppContainer from 'containers/App'

import 'ant-design-pro/dist/ant-design-pro.css'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/mdn-like.css'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import '@uppy/dashboard/dist/style.css'
import 'styles/bpmn.scss'
import 'styles/main.scss'

const token = localStorage.getItem('token')

if (token !== null && token !== 'undefined') {
  store.dispatch.auth.loginWithToken(token)
}

window.addEventListener('storage', function (e) {
  if (e.key === 'token' && e.newValue === null) {
    store.dispatch.auth.logoutUser()
  }
})

const LoginRoute = PublicOnlyRedir(LoginLayout(LoginContainer))
const RootRoute = AuthOnlyRedir(AppContainer)

const target = document.getElementById('root')

const Root = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path='/login' component={LoginRoute} />
          <Route path='' component={RootRoute} />
        </Switch>
      </Router>
    </Provider>
  )
}

render(<Root />, target)
