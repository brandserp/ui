const { override, fixBabelImports, addLessLoader, addBabelPlugin } = require('customize-cra')
const antdVars = require('./antd-override.json')

module.exports = override(
  addBabelPlugin('react-hot-loader/babel'),
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    modifyVars: antdVars,
    javascriptEnabled: true
  })
)
